#include "effects.h"
#include <stddef.h>

typedef struct ColorHsv {
    unsigned char h;
    unsigned char s;
    unsigned char v;
} ColorHsv;

#define MAX_PERCENT        100
#define MAX_HSV_HUE        255
#define MAX_HSV_VALUE      255
#define MAX_HSV_SATURATION 255

static void fromHsv(const ColorHsv * const hsv, EffectColor * color);
static int  wrap(int v, int ubound, int lbound);

inline static unsigned char percent(unsigned int vmax, unsigned int v)
{
    return (v % vmax) * MAX_PERCENT / vmax;
}

inline static int step(int x0, int x1, unsigned char percent)
{
    return x0 + ((x1 - x0) * percent / MAX_PERCENT);
}

inline static void stepColor(EffectColor * color0, EffectColor * color1, EffectColor * resultColor, unsigned char percent)
{
    resultColor->r = step(color0->r, color1->r, percent);
    resultColor->g = step(color0->g, color1->g, percent);
    resultColor->b = step(color0->b, color1->b, percent);
}

void effectSwitch(EffectState * state, SwitchEffectSettings * settings)
{
    if (state == NULL || state->outputCallback == NULL || state->pointCount == 0 ||
        settings == NULL || settings->colors == NULL || settings->colorCount == 0)
        return;

    unsigned char progress = percent(settings->duration, state->timestamp);
    if (settings->reverse)
        progress = MAX_PERCENT - progress;

    if (settings->lateral == 1) {
        for (unsigned int point = 0; point < state->pointCount; point++) {
            unsigned char pointProgress;

            if (settings->mirror == 1) {
                if (point > state->pointCount / 2)
                    pointProgress = percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0));
                else
                    pointProgress = MAX_PERCENT - percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0));
            } else {
                pointProgress = percent(state->pointCount, point);
            }

            unsigned char colorProgress = (unsigned char)wrap(pointProgress + progress, MAX_PERCENT, 0);
            unsigned int  currentColor  = colorProgress * settings->colorCount / MAX_PERCENT % settings->colorCount;
            state->outputCallback(&settings->colors[currentColor], point, state->userData);
        }
    } else {
        unsigned int currentColor = (settings->colorCount * progress / MAX_PERCENT) % settings->colorCount;
        for (unsigned int point = 0; point < state->pointCount; point++) {
            state->outputCallback(&settings->colors[currentColor], point, state->userData);
        }
    }
}

void effectFade(EffectState * state, FadeEffectSettings * settings)
{
    if (state == NULL || state->outputCallback == NULL || state->pointCount == 0 ||
        settings == NULL || settings->colors == NULL || settings->colorCount == 0)
        return;

    unsigned char progress = percent(settings->duration, state->timestamp);

    if (settings->reverse == 1)
        progress = MAX_PERCENT - progress;

    unsigned int colorDuration = settings->duration / settings->colorCount;
    unsigned char fxPercentPerColor = MAX_PERCENT / settings->colorCount;
    EffectColor color;

    if (settings->lateral == 1) {
        for (unsigned int point = 0; point < state->pointCount; point++) {
            unsigned char pointProgress;
            if (settings->mirror == 1) {
                if (point > state->pointCount / 2)
                    pointProgress = wrap(percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0)) + progress, MAX_PERCENT, 0);
                else
                    pointProgress = wrap(MAX_PERCENT - percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0)) + progress, MAX_PERCENT, 0);
            } else {
                pointProgress = wrap(percent(state->pointCount, point) + progress, MAX_PERCENT, 0);
            }

            unsigned int colorIndex = pointProgress / fxPercentPerColor;
            unsigned int prevColorIndex = colorIndex + 1;

            if (prevColorIndex >= settings->colorCount)
                prevColorIndex = 0;
            if (colorIndex >= settings->colorCount)
                colorIndex = settings->colorCount - 1;

            unsigned char colorProgress = MAX_PERCENT - (pointProgress - fxPercentPerColor * colorIndex) * settings->colorCount;

            stepColor(&settings->colors[prevColorIndex], &settings->colors[colorIndex], &color, colorProgress);
            state->outputCallback(&color, point, state->userData);
        }
    } else {
        unsigned int currentColor = state->timestamp % settings->duration / colorDuration;
        if (settings->reverse == 1)
            currentColor = settings->colorCount - currentColor - 1;

        unsigned int nextColor = settings->reverse == 1 ? ((int)(currentColor - 1) < 0 ? settings->colorCount - 1 : currentColor - 1)
                                                        : (currentColor + 1 >= settings->colorCount ? 0 : currentColor + 1);
        unsigned int colorTime = state->timestamp % colorDuration;
        unsigned char currentColorProgress = percent(colorDuration, colorTime);

        stepColor(&settings->colors[currentColor], &settings->colors[nextColor], &color, currentColorProgress);

        for (unsigned int point = 0; point < state->pointCount; point++)
            state->outputCallback(&color, point, state->userData);
    }
}

void effectChase(EffectState * state, ChaseEffectSettings * settings)
{
    unsigned char progress = percent(settings->duration, state->timestamp);
    if (settings->reverse == 1)
        progress = MAX_PERCENT - progress;

    unsigned int colorDuration = settings->duration / settings->colorCount;
    unsigned int fxPercentPerColor = MAX_PERCENT / settings->colorCount;
    EffectColor color;
    static const EffectColor blackColor = {.r = 0, .g = 0, .b = 0};

    if (settings->lateral == 1) {
      for (unsigned int point = 0; point < state->pointCount; point++) {
        int pointProgress;

        if (settings->mirror == 1) {
          if (point > state->pointCount / 2)
            pointProgress = wrap(percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0)) + progress, MAX_PERCENT, 0);
          else
            pointProgress = wrap(MAX_PERCENT - percent(state->pointCount, wrap(point * 2, state->pointCount - 1, 0)) + progress, MAX_PERCENT, 0);;
        } else {
          pointProgress = wrap(percent(state->pointCount, point) + progress, MAX_PERCENT, 0);
        }

        unsigned int colorIndex    = pointProgress / fxPercentPerColor;

        if (colorIndex >= settings->colorCount)
          colorIndex = settings->colorCount - 1;

        unsigned char colorProgress = settings->reverse == 1 ? (pointProgress - fxPercentPerColor * colorIndex) * settings->colorCount
                                                             : MAX_PERCENT - (pointProgress - fxPercentPerColor * colorIndex) * settings->colorCount;

        stepColor((EffectColor *)&blackColor, &settings->colors[colorIndex], &color, colorProgress);
        state->outputCallback(&color, point, state->userData);
      }
    } else {
      unsigned int currentColor = state->timestamp % settings->duration / colorDuration;

      if (settings->reverse == 1)
        currentColor = settings->colorCount - currentColor - 1;

      unsigned int colorTime = state->timestamp % colorDuration;
      unsigned char currentColorProgress = MAX_PERCENT - percent(colorDuration, colorTime);

      stepColor((EffectColor *)&blackColor, &settings->colors[currentColor], &color, currentColorProgress);

        for (unsigned int point = 0; point < state->pointCount; point++)
            state->outputCallback(&color, point, state->userData);
    }
}

void effectRainbow(EffectState * state, RainbowEffectSettings * settings)
{
    if (state == NULL || state->outputCallback == NULL || state->pointCount == 0 ||
        settings == NULL)
        return;

    unsigned char progress = percent(settings->duration, state->timestamp);
    unsigned char hueOffset = 0;

    if (settings->lateral != 0)
        hueOffset = (progress * state->pointCount) / MAX_PERCENT;

    ColorHsv hsv = {
                     .h = (MAX_HSV_HUE * progress / MAX_PERCENT),
                     .s = MAX_HSV_SATURATION,
                     .v = MAX_HSV_VALUE
                   };
    EffectColor color;
    fromHsv(&hsv, &color);

    for (unsigned int point = 0; point < state->pointCount; point++) {
        if (settings->lateral != 0) {
            unsigned int pointIndex = point + hueOffset;
            if (pointIndex >= state->pointCount)
                pointIndex = pointIndex - state->pointCount;
            unsigned char pointIndexProgress = pointIndex * MAX_PERCENT / state->pointCount;
            hsv.h = (MAX_HSV_HUE * pointIndexProgress / MAX_PERCENT);
            fromHsv(&hsv, &color);
        }
        state->outputCallback(&color, point, state->userData);
    }
}

static int wrap(int v, int ubound, int lbound)
{
    int range = ubound - lbound + 1;
    if (v < lbound)
        v += range * ((lbound - v) / range + 1);
    return lbound + (v - lbound) % range;
}

static void fromHsv(const ColorHsv * const hsv, EffectColor * color)
{
    unsigned char region, remainder, p, q, t;

    if (hsv->s == 0)
    {
        color->r = hsv->v;
        color->g = hsv->v;
        color->b = hsv->v;
    }

    region = hsv->h / 43;
    remainder = (hsv->h - (region * 43)) * 6;

    p = (hsv->v * (255 - hsv->s)) >> 8;
    q = (hsv->v * (255 - ((hsv->s * remainder) >> 8))) >> 8;
    t = (hsv->v * (255 - ((hsv->s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
        case 0:
            color->r = hsv->v; color->g = t; color->b = p;
            break;
        case 1:
            color->r = q; color->g = hsv->v; color->b = p;
            break;
        case 2:
            color->r = p; color->g = hsv->v; color->b = t;
            break;
        case 3:
            color->r = p; color->g = q; color->b = hsv->v;
            break;
        case 4:
            color->r = t; color->g = p; color->b = hsv->v;
            break;
        default:
            color->r = hsv->v; color->g = p; color->b = q;
            break;
    }
}
