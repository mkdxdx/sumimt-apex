#pragma once
#include <stdint.h>
#include <stdbool.h>

typedef enum ConfigKey {
    ConfigKeyChannelCount,
    ConfigKeyGateOverride,
    ConfigKeyCommChannel,
    ConfigKeyDriverType,
    ConfigKeyCount
} ConfigKey;

typedef enum ConfigDriverType {
    ConfigDriverTypeNone,
    ConfigDriverTypePwm,
    ConfigDriverTypePixel1,
    ConfigDriverTypePixel2,
    ConfigDriverTypeSerial1,
    ConfigDriverTypeSerial2,
    ConfigDriverTypeCount
} ConfigDriverType;

typedef enum DataId {
    DataIdChannelData    = 0x01,
    DataIdConfigPairList = 0x02,
    DataIdEffect         = 0x03,
    DataIdStatus         = 0x04,
} DataId;

typedef enum EffectId {
    EffectIdNone,
    EffectIdFill,
    EffectIdSwitch,
    EffectIdFade,
    EffectIdChase,
    EffectIdRainbow,
    EffectIdCount
} EffectId;

#pragma pack(push,1)
typedef struct ChannelDataHeader {
    uint32_t dataOffset;
    uint32_t dataSize;
} ChannelDataHeader;

typedef struct ConfigPair {
    uint32_t configKey;
    uint32_t configValue;
} ConfigPair;

typedef struct ConfigPairListHeader {
    uint8_t    configPairCount;
} ConfigPairListHeader;

typedef struct EffectDataHeader {
    uint8_t  effectId;
    uint32_t effectPeriod;
    uint32_t paletteColorCount;
    uint32_t paletteColorOffset;
    uint8_t  mirrored: 1;
    uint8_t  reverse : 1;
    uint8_t  lateral : 1;
    uint8_t  reserved: 5;
} EffectDataHeader;

typedef struct EffectDataColor {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} EffectDataColor;

typedef struct StatusData {
    uint8_t batteryStatePercents;
} StatusData;

typedef struct DataHeader {
    uint8_t dataId;
} DataHeader;
#pragma pack(pop)

#define PROTOCOL_CHANNEL_DATA_SIZE(N)     ( sizeof(DataHeader) + \
                                            sizeof(ChannelDataHeader) + \
                                            (N) )
#define PROTOCOL_CONFIG_PAIR_LIST_SIZE(N) ( sizeof(DataHeader) + \
                                            sizeof(ConfigPairListHeader) + \
                                            sizeof(ConfigPair) * (N) )

#define PROTOCOL_EFFECT_DATA_SIZE(N)      ( sizeof(DataHeader) + \
                                            sizeof(EffectDataHeader) + \
                                            sizeof(EffectDataColor) * (N) )
