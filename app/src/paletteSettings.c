#include "paletteSettings.h"
#include "guiElements.h"
#include "settings.h"
#include "appletManager.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define GLOBAL_X_OFFSET (2U)

enum GuiEvent {
    GUI_EVENT_DRAW_ROOT_COLOR_LIST,
    GUI_EVENT_DRAW_ROOT_COLOR_PICKER,
    GUI_EVENT_DRAW_SETTINGS_ALL,
    GUI_EVENT_DRAW_SETTINGS_COLOR_COUNT,
    GUI_EVENT_DRAW_SETTINGS_COLOR_PICKER,
    GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_R,
    GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_G,
    GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_B,
    GUI_EVENT_DRAW_SETTINGS_IMPORT_PALETTE_INDEX,
    GUI_EVENT_DRAW_DIAL_HINT,
    GUI_EVENT_DRAW_ENTER_HINT,

    GUI_EVENT_DRAW_FOCUS_BIT = 0x80U,
};

static const Point2d PosPalette = {GLOBAL_X_OFFSET, 2};
static const Point2d PosColorCount = {GLOBAL_X_OFFSET, 2};
static const Point2d PosColorPicker = {GLOBAL_X_OFFSET, 14};
static const Point2d PosControlR = {GLOBAL_X_OFFSET, 26};
static const Point2d PosControlG = {GLOBAL_X_OFFSET + 38, 26};
static const Point2d PosControlB = {GLOBAL_X_OFFSET + 76, 26};
static const Point2d PosImportPaletteIndex = {GLOBAL_X_OFFSET, 38};
static const Point2d PosDialHint = {GLOBAL_X_OFFSET, 116};
static const Point2d PosEnterHint = {GLOBAL_X_OFFSET + 64, 116};

static const Point2d SizHalfLine = {64, 12};
static const Point2d SizFullLine = {128, 12};
static const Point2d SizFiveChars = {38, 12};

static enum {
    MenuLevelRoot,
    MenuLevelPaletteSettings,
} menuLevel = MenuLevelRoot;
static uint32_t paletteIndex  = 0;
static enum {
    DIAL_STATE_HINT_NONE,
    DIAL_STATE_HINT_SELECT,
    DIAL_STATE_HINT_SAVE,
    DIAL_STATE_HINT_IMPORT
} dialStateHint = DIAL_STATE_HINT_SELECT;
static enum {
    ENTER_STATE_HINT_NONE,
    ENTER_STATE_HINT_REVERT,
} enterStateHint = ENTER_STATE_HINT_NONE;
static enum PaletteSettings {
    PaletteSettingsColorCount,
    PaletteSettingsColorSelect,
    PaletteSettingsControlR,
    PaletteSettingsControlG,
    PaletteSettingsControlB,
    PaletteSettingsImport,
    PaletteSettingsCount
} paletteSettings = PaletteSettingsColorCount;
static uint32_t colorIndex = 0;
static uint32_t importPaletteIndex = 0;
static Palette paletteSettingsSnapshot;

static bool isConfigChanged(void)
{
    Palette settings;
    settingsGetPalette(paletteIndex, &settings);
    return 0 != memcmp(&settings, &paletteSettingsSnapshot, sizeof(settings));
}

static void appletOnEnter(void)
{
    menuLevel = MenuLevelRoot;
    dialStateHint = DIAL_STATE_HINT_SELECT;
    enterStateHint = ENTER_STATE_HINT_NONE;
    paletteIndex = 0;
    paletteSettings = PaletteSettingsColorCount;
    importPaletteIndex = 0;
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_LIST, 0);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER | GUI_EVENT_DRAW_FOCUS_BIT,
                               paletteIndex);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2)
{

}

static void appletOnGui(uint32_t arg1, uint32_t arg2)
{
    bool inFocus = (arg1 & GUI_EVENT_DRAW_FOCUS_BIT) != 0;
    arg1 = arg1 & (~GUI_EVENT_DRAW_FOCUS_BIT);
    bool drawAll = false;

    ColorPickerSettings colorPicker;
    Palette palette;
    Point2d position;
    Palette *palSnap = &paletteSettingsSnapshot;

    switch ((enum GuiEvent)arg1) {
    case GUI_EVENT_DRAW_ROOT_COLOR_LIST:
        guiElementFillBackground();
        colorPicker.isHsv = false;
        colorPicker.inFocus = false;
        colorPicker.size = &SizFullLine;
        colorPicker.colorElementHeight = 11;
        colorPicker.colorElementWidth  = 12;
        colorPicker.colorIndex = 0;
        position.x = PosPalette.x;
        colorPicker.position = &position;
        for (uint32_t paletteIdx = 0; paletteIdx < PALETTE_COUNT; paletteIdx++) {
            position.y = PosPalette.y + colorPicker.colorElementHeight * paletteIdx;
            settingsGetPalette(paletteIdx, &palette);
            colorPicker.rgbColors = palette.color;
            colorPicker.colorCount = palette.colorCount;
            guiElementColorPicker(&colorPicker);
        }
        break;
    case GUI_EVENT_DRAW_ROOT_COLOR_PICKER:
        colorPicker.isHsv = false;
        colorPicker.inFocus = inFocus;
        colorPicker.size = &SizFullLine;
        colorPicker.colorElementHeight = 11;
        colorPicker.colorElementWidth  = 12;
        position.x = PosPalette.x;
        position.y = PosPalette.y + colorPicker.colorElementHeight * arg2;
        colorPicker.position = &position;
        settingsGetPalette(arg2, &palette);
        colorPicker.colorIndex = palette.colorCount - 1;
        colorPicker.rgbColors = palette.color;
        colorPicker.colorCount = palette.colorCount;
        guiElementColorPicker(&colorPicker);
        break;
    case GUI_EVENT_DRAW_SETTINGS_ALL:
        drawAll = true;
        inFocus = false;
        guiElementFillBackground();

    case GUI_EVENT_DRAW_SETTINGS_COLOR_COUNT:
        guiElementLabeledNumericValue(&palSnap->colorCount, NumericValueTypeU32, "Color count:", 
                                      inFocus, NumericBaseDec, &PosColorCount, &SizFullLine);
        if (!drawAll) {
            break;
        }
        
    case GUI_EVENT_DRAW_SETTINGS_COLOR_PICKER:
        colorPicker.isHsv = false;
        colorPicker.inFocus = inFocus;
        colorPicker.size = &SizFullLine;
        colorPicker.colorElementHeight = 11;
        colorPicker.colorElementWidth  = 12;
        colorPicker.position = &PosColorPicker;
        colorPicker.colorIndex = colorIndex;
        colorPicker.rgbColors = palSnap->color;
        colorPicker.colorCount = palSnap->colorCount;
        guiElementColorPicker(&colorPicker);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_R:
        guiElementLabeledNumericValue(&palSnap->color[colorIndex].r, NumericValueTypeU8, "R:", 
                                      inFocus, NumericBaseDec, &PosControlR, &SizFiveChars);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_G:
        guiElementLabeledNumericValue(&palSnap->color[colorIndex].g, NumericValueTypeU8, "G:", 
                                      inFocus, NumericBaseDec, &PosControlG, &SizFiveChars);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_B:
        guiElementLabeledNumericValue(&palSnap->color[colorIndex].b, NumericValueTypeU8, "B:", 
                                      inFocus, NumericBaseDec, &PosControlB, &SizFiveChars);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_SETTINGS_IMPORT_PALETTE_INDEX:
        guiElementLabeledNumericValue(&importPaletteIndex, NumericValueTypeU32, "Import from:", 
                                      inFocus, NumericBaseDec, &PosImportPaletteIndex, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DIAL_HINT:
        switch (dialStateHint) {
        case DIAL_STATE_HINT_NONE:
            guiElementLabel("o   None|", false, &PosDialHint, NULL);
            break;
        case DIAL_STATE_HINT_SELECT:
            guiElementLabel("o Select|", false, &PosDialHint, NULL);
            break;
        case DIAL_STATE_HINT_IMPORT:
            guiElementLabel("o Import|", false, &PosDialHint, NULL);
            break;
        case DIAL_STATE_HINT_SAVE:
            guiElementLabel("o   Save|", false, &PosDialHint, NULL);
            break;
        }
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_ENTER_HINT:
        switch (enterStateHint) {
        case ENTER_STATE_HINT_NONE:
            guiElementLabel("K2   Exit", false, &PosEnterHint, NULL);
            break;
        case ENTER_STATE_HINT_REVERT:
            guiElementLabel("K2 Revert", false, &PosEnterHint, NULL);
            break;
        }
        // fallthrough ends here
        break;
    }
}

static void queuePaletteSettingsControlDraw(enum PaletteSettings index, bool focus)
{
    uint32_t event = 0;
    uint32_t arg = 0;
    switch (index) {
    case PaletteSettingsColorCount:
        event = GUI_EVENT_DRAW_SETTINGS_COLOR_COUNT;
        break;
    case PaletteSettingsColorSelect:
        event = GUI_EVENT_DRAW_SETTINGS_COLOR_PICKER;
        break;
    case PaletteSettingsControlR:
        event = GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_R;
        break;
    case PaletteSettingsControlG:
        event = GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_G;
        break;
    case PaletteSettingsControlB:
        event = GUI_EVENT_DRAW_SETTINGS_COLOR_CONTROL_B;
        break;
    case PaletteSettingsImport:
        event = GUI_EVENT_DRAW_SETTINGS_IMPORT_PALETTE_INDEX;
        break;
    default:
        return;
    }
    if (focus) {
        event |= GUI_EVENT_DRAW_FOCUS_BIT;
    }
    appletManagerQueueGuiEvent(event, arg);
}

static void onButtonDown(void)
{
    uint32_t prevPaletteIndex = paletteIndex;
    enum PaletteSettings prevPaletteSettings = paletteSettings;
    switch (menuLevel) {
    case MenuLevelRoot:
        if (paletteIndex + 1 < PALETTE_COUNT) {
            paletteIndex++;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER, prevPaletteIndex);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER | GUI_EVENT_DRAW_FOCUS_BIT,
                                       paletteIndex);
        }
        break;
    case MenuLevelPaletteSettings:
        if (paletteSettings + 1 < PaletteSettingsCount) {
            paletteSettings++;
            queuePaletteSettingsControlDraw(prevPaletteSettings, false);
            queuePaletteSettingsControlDraw(paletteSettings, true);
            if (paletteSettings == PaletteSettingsImport) {
                dialStateHint = DIAL_STATE_HINT_IMPORT;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
        }
        break;
    default:
        return;
    }
}

static void onButtonUp(void)
{
    uint32_t prevPaletteIndex = paletteIndex;
    enum PaletteSettings prevPaletteSettings = paletteSettings;
    switch (menuLevel) {
    case MenuLevelRoot:
        if ((int32_t)paletteIndex - 1 >= 0) {
            paletteIndex--;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER, prevPaletteIndex);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER | GUI_EVENT_DRAW_FOCUS_BIT,
                                       paletteIndex);
        }
        break;
    case MenuLevelPaletteSettings:
        if ((int32_t)paletteSettings - 1 >= 0) {
            paletteSettings--;
            queuePaletteSettingsControlDraw(prevPaletteSettings, false);
            queuePaletteSettingsControlDraw(paletteSettings, true);
            if (paletteSettings == PaletteSettingsImport) {
                dialStateHint = DIAL_STATE_HINT_IMPORT;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
        }
        break;
    default:
        return;
    }
}

static void onButtonDial(void)
{
    switch (dialStateHint) {
    case DIAL_STATE_HINT_SELECT:
        switch(menuLevel) {
        case MenuLevelRoot:
            menuLevel = MenuLevelPaletteSettings;
            dialStateHint = DIAL_STATE_HINT_NONE;
            paletteSettings = PaletteSettingsColorCount;
            settingsGetPalette(paletteIndex, &paletteSettingsSnapshot);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_SETTINGS_ALL, 0);
            queuePaletteSettingsControlDraw(paletteSettings, true);
            break;
        }
        break;
    case DIAL_STATE_HINT_SAVE:
        settingsSetPalette(paletteIndex, &paletteSettingsSnapshot);
        dialStateHint = DIAL_STATE_HINT_NONE;
        enterStateHint = ENTER_STATE_HINT_NONE;
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        break;
    case DIAL_STATE_HINT_IMPORT:
        settingsGetPalette(importPaletteIndex, &paletteSettingsSnapshot);
        if (isConfigChanged()) {
            dialStateHint = DIAL_STATE_HINT_SAVE;
            enterStateHint = ENTER_STATE_HINT_REVERT;
        } else {
            dialStateHint = DIAL_STATE_HINT_NONE;
            enterStateHint = ENTER_STATE_HINT_NONE;
        }
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_SETTINGS_ALL, 0);
        queuePaletteSettingsControlDraw(paletteSettings, true);
        break;
    }
}

static void onButtonEnter(void)
{
    bool redrawHints = false;
    switch (enterStateHint) {
    case ENTER_STATE_HINT_NONE:
        switch (menuLevel) {
        case MenuLevelRoot:
            appletManagerQueueAppletExit();
            break;
        case MenuLevelPaletteSettings:
            menuLevel = MenuLevelRoot;
            dialStateHint = DIAL_STATE_HINT_SELECT;
            enterStateHint = ENTER_STATE_HINT_NONE;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_LIST, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ROOT_COLOR_PICKER | GUI_EVENT_DRAW_FOCUS_BIT,
                                       paletteIndex);
            redrawHints = true;
            break;
        }
        break;
    case ENTER_STATE_HINT_REVERT:
        if (menuLevel == MenuLevelRoot) {
            break;
        }
        settingsGetPalette(paletteIndex, &paletteSettingsSnapshot);
        enterStateHint = ENTER_STATE_HINT_NONE;
        dialStateHint = DIAL_STATE_HINT_NONE;
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_SETTINGS_ALL, 0);
        queuePaletteSettingsControlDraw(paletteSettings, true);
        break;
    }

    if (redrawHints) {
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
    }
}

static void onEncoder(int32_t delta)
{
    if (menuLevel == MenuLevelRoot) {
        return;
    }

    Palette *palette = &paletteSettingsSnapshot;
    bool recheckConfig = false;
    bool redrawControl = false;

    switch (paletteSettings) {
    case PaletteSettingsColorCount:
        if ((int32_t)palette->colorCount + delta < PALETTE_SIZE
            && (int32_t)palette->colorCount + delta >= 0) {
            palette->colorCount += delta;;
            recheckConfig = true;
            redrawControl = true;
            queuePaletteSettingsControlDraw(PaletteSettingsColorSelect, false);
        }
        break;
    case PaletteSettingsColorSelect:
        if ((int32_t)colorIndex + delta < palette->colorCount
            && (int32_t)colorIndex + delta >= 0) {
            colorIndex += delta;
            redrawControl = true;
            queuePaletteSettingsControlDraw(PaletteSettingsControlR, false);
            queuePaletteSettingsControlDraw(PaletteSettingsControlG, false);
            queuePaletteSettingsControlDraw(PaletteSettingsControlB, false);
        }
        break;
    case PaletteSettingsControlR:
        if ((int32_t)palette->color[colorIndex].r + delta * 5 <= 255
            && (int32_t)palette->color[colorIndex].r + delta * 5 >= 0) {
            palette->color[colorIndex].r += delta * 5;
            recheckConfig = true;
            redrawControl = true;
            queuePaletteSettingsControlDraw(PaletteSettingsColorSelect, false);
        }
        break;
    case PaletteSettingsControlG:
        if ((int32_t)palette->color[colorIndex].g + delta * 5 <= 255
            && (int32_t)palette->color[colorIndex].g + delta * 5 >= 0) {
            palette->color[colorIndex].g += delta * 5;
            recheckConfig = true;
            redrawControl = true;
            queuePaletteSettingsControlDraw(PaletteSettingsColorSelect, false);
        }
        break;
    case PaletteSettingsControlB:
        if ((int32_t)palette->color[colorIndex].b + delta * 5 <= 255
            && (int32_t)palette->color[colorIndex].b + delta * 5 >= 0) {
            palette->color[colorIndex].b += delta * 5;
            recheckConfig = true;
            queuePaletteSettingsControlDraw(PaletteSettingsColorSelect, false);
            redrawControl = true;
        }
        break;
    case PaletteSettingsImport:
        if ((int32_t)importPaletteIndex + delta < PALETTE_COUNT
            && (int32_t)importPaletteIndex + delta >= 0) {
            importPaletteIndex += delta;
            dialStateHint = DIAL_STATE_HINT_IMPORT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            redrawControl = true;
        }
        break;
    }

    if (redrawControl) {
        queuePaletteSettingsControlDraw(paletteSettings, true);
    }
    if (recheckConfig) {
        if (isConfigChanged()) {
            dialStateHint = DIAL_STATE_HINT_SAVE;
            enterStateHint = ENTER_STATE_HINT_REVERT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        } else {
            if (dialStateHint != DIAL_STATE_HINT_NONE) {
                dialStateHint = DIAL_STATE_HINT_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
            if (enterStateHint != ENTER_STATE_HINT_NONE) {
                enterStateHint = ENTER_STATE_HINT_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            }
        }
    }


}

static void appletOnInput(InputEvent evt)
{
    switch (evt.type) {
    case EventButtonDown:
        if (evt.buttonState) {
            onButtonDown();
        } 
        break;
    case EventButtonUp:
        if (evt.buttonState) {
            onButtonUp();
        }
        break;
    case EventButtonDial:
        if (evt.buttonState) {
            onButtonDial();
        }
        break;
    case EventButtonEnter:
        if (evt.buttonState) {
            onButtonEnter();
        }
        break;
    case EventEncoder:
        onEncoder(evt.encoderDelta);
        break;
    }
}

static void appletOnExit(void)
{

}

void paletteSettingsStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}