#pragma once

#define ILI9341_SPI_INSTANCE 3
#define ILI9341_DC_PIN 19
#define ILI9341_MISO_PIN 0xFF
#define ILI9341_MOSI_PIN 20
#define ILI9341_SCK_PIN 21
#define ILI9341_SS_PIN 17
#define ILI9341_RESX_PIN 15
#define ILI9341_LED_PIN 22
#define ILI9341_HEIGHT 128
#define ILI9341_WIDTH 128

// this function uses OS primitive to wait until
// lcdSpiOsTransferDone will be called (usually 
// waits on semaphore)
extern void lcdSpiOsTransferWait(void);

// this function will be called once spi transaction is done
// called from ISR context
extern void lcdSpiOsTransferDone(void);

// initializes OS primitive for wait/done operations
// usually just creates a semaphore
extern void lcdSpiOsTransferInit(void);

// cleanup after lcd uninit is called
// can be used to delete semaphore
extern void lcdSpiOsTransferUninit(void);

// use os means to delay execution for amount of ms
extern void lcdOsDelayMs(unsigned int ms);