#pragma once

typedef enum TaskPriority {
    TaskPriorityGui = 1,
    TaskPriorityButton,
    TaskPriorityProcess,
    TaskPriorityCount
} TaskPriority;
