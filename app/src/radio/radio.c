#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "radio.h"

#include "sdk_common.h"
#include "nrf.h"
#include "nrf_esb.h"
#include "nrf_error.h"
#include "nrf_esb_error_codes.h"
#include "app_error.h"

enum Pipe {
    PIPE_TX = 0,
    PIPE_RX = 1,
    PIPE_COUNT
};

static void setAddress(const uint8_t address[ADDRESS_LENGTH], enum Pipe pipe)
{
    uint8_t base[ADDRESS_LENGTH - 1];
    uint8_t prefix;
    uint32_t err;

    memcpy(base, address, sizeof(base));
    prefix = address[ADDRESS_LENGTH - 1];

    switch (pipe) {
    case PIPE_TX:
        err = nrf_esb_set_base_address_0(base);
        break;
    case PIPE_RX:
        err = nrf_esb_set_base_address_1(base);
        break;
    default:
        return;
    }
    APP_ERROR_CHECK(err);

    err = nrf_esb_update_prefix(pipe, prefix);
    APP_ERROR_CHECK(err);
}

static_assert(PIPE_COUNT <= NRF_ESB_PIPE_COUNT,
              "Pipe count exceed configuration pipe count");
static_assert(RADIO_DATA_MAX_LENGTH <= NRF_ESB_MAX_PAYLOAD_LENGTH, 
              "Radio packet cannot be larger than maximum ESB payload");

static void nrf_esb_event_handler(nrf_esb_evt_t const * p_event);

static volatile RadioDataReceivedCallback receivedCb = NULL;
static RadioDataSentCallback sentCb = NULL;

void radioInit(RadioDataReceivedCallback rxCb, RadioDataSentCallback txCb, 
                bool slave,
                const uint8_t ownAddress[ADDRESS_LENGTH],
                uint8_t channel)
{
    nrf_esb_stop_rx();
    while (!nrf_esb_is_idle()) {}

    receivedCb = rxCb;
    sentCb = txCb;

    uint32_t err_code;
    nrf_esb_config_t nrf_esb_config         = NRF_ESB_DEFAULT_CONFIG;

    nrf_esb_config.radio_irq_priority = 5;
    nrf_esb_config.event_irq_priority = 6;
    nrf_esb_config.event_handler            = nrf_esb_event_handler;
    nrf_esb_config.protocol = NRF_ESB_PROTOCOL_ESB_DPL;
    nrf_esb_config.mode = slave ? NRF_ESB_MODE_PRX : NRF_ESB_MODE_PTX;
    nrf_esb_config.payload_length = RADIO_DATA_MAX_LENGTH;
    // nrf_esb_config.bitrate = NRF_ESB_BITRATE_2MBPS;
    nrf_esb_config.bitrate = NRF_ESB_BITRATE_1MBPS;
    err_code = nrf_esb_init(&nrf_esb_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_esb_set_address_length(ADDRESS_LENGTH);
    APP_ERROR_CHECK(err_code);
    setAddress(ownAddress, PIPE_RX);
    err_code = nrf_esb_set_rf_channel(channel);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_esb_enable_pipes(slave ? 1 << PIPE_RX : 1 << PIPE_TX);
    if (slave) {
        err_code = nrf_esb_start_rx();
        APP_ERROR_CHECK(err_code);
    }
    APP_ERROR_CHECK(nrf_esb_flush_tx());
    APP_ERROR_CHECK(nrf_esb_flush_rx());
}

bool radioSendData(const uint8_t data[RADIO_DATA_MAX_LENGTH], uint32_t size)
{
    if (size > RADIO_DATA_MAX_LENGTH) {
        return false;
    }

    uint32_t err;
    nrf_esb_payload_t txPayload = {
        .length = size,
        .pipe = PIPE_TX,
        .noack = false,
    };

    memcpy(txPayload.data, data, size);
    err = nrf_esb_write_payload(&txPayload);
    APP_ERROR_CHECK(err);
    return err == NRF_SUCCESS;
}

void radioSetTxAddress(const uint8_t address[ADDRESS_LENGTH])
{
    setAddress(address, PIPE_TX);
}

void radioWriteAckPayload(const uint8_t data[RADIO_DATA_MAX_LENGTH], uint32_t size)
{
    if (size > RADIO_DATA_MAX_LENGTH) {
        size = RADIO_DATA_MAX_LENGTH;
    }

    nrf_esb_payload_t ackPld = {
        .pipe = PIPE_RX,
        .length = size,
    };
    memcpy(ackPld.data, data, size);
    APP_ERROR_CHECK(nrf_esb_write_payload(&ackPld));
}

void radioDisable(void)
{
    APP_ERROR_CHECK(nrf_esb_disable());
}

static void nrf_esb_event_handler(nrf_esb_evt_t const * p_event)
{
    nrf_esb_payload_t rxPayload;
    uint32_t ret;

    switch (p_event->evt_id)
    {

    case NRF_ESB_EVENT_TX_SUCCESS:
        if (sentCb) {
            sentCb(true);
        }
        break;

    case NRF_ESB_EVENT_TX_FAILED:
        (void) nrf_esb_flush_tx();
        if (sentCb) {
            sentCb(false);
        }
        break;

    case NRF_ESB_EVENT_RX_RECEIVED:
        ret = nrf_esb_read_rx_payload(&rxPayload);
        if (ret == NRF_SUCCESS && receivedCb != NULL) {
            receivedCb(rxPayload.data, rxPayload.length);
        }
        break;

    }
}
