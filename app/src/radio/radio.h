#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define ADDRESS_LENGTH (5U)
#define RADIO_DATA_MAX_LENGTH (252U)

typedef void (*RadioDataReceivedCallback)(const uint8_t data[RADIO_DATA_MAX_LENGTH], 
                                          size_t size);
typedef void (*RadioDataSentCallback)(bool success);

void radioInit(RadioDataReceivedCallback rxCb, RadioDataSentCallback txCb, 
                bool slave,
                const uint8_t ownAddress[ADDRESS_LENGTH],
                uint8_t channel);
bool radioSendData(const uint8_t data[RADIO_DATA_MAX_LENGTH], uint32_t size);
void radioSetTxAddress(const uint8_t address[ADDRESS_LENGTH]);
void radioWriteAckPayload(const uint8_t data[RADIO_DATA_MAX_LENGTH], uint32_t size);
void radioDisable(void);