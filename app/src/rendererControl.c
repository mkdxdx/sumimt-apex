#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "common.h"

#include "rendererControl.h"
#include "rendererControlSettings.h"
#include "guiElements.h"
#include "settings.h"
#include "protocol.h"
#include "renderer.h"
#include "effects.h"
#include "rendererCommon.h"
#include "displayManager.h"
#include "appletManager.h"

#define GLOBAL_X_OFFSET (4U)

#define EFFECT_SETTINGS_X   0
#define EFFECT_SETTINGS_Y   22
#define BRUSH_OPTIONS_X     2
#define BRUSH_OPTIONS_Y     110

enum GuiEvent {
    GUI_EVENT_CLEAR_SCREEN,
    GUI_EVENT_DRAW_ALL,
    GUI_EVENT_DRAW_RENDERER_INDEX,
    GUI_EVENT_DRAW_RENDER_MODE,
    GUI_EVENT_DRAW_EFFECT_INDEX,
    GUI_EVENT_DRAW_EFFECT_SETTINGS,
    GUI_EVENT_DRAW_EFFECT_TYPE,
    GUI_EVENT_DRAW_PALETTE_INDEX,
    GUI_EVENT_DRAW_PALETTE,
    GUI_EVENT_DRAW_EFFECT_DURATION,
    GUI_EVENT_DRAW_EFFECT_LATERAL,
    GUI_EVENT_DRAW_EFFECT_REVERSE,
    GUI_EVENT_DRAW_EFFECT_MIRROR,
    GUI_EVENT_DRAW_VIEWPORT_INDEX,
    GUI_EVENT_DRAW_DIAL_HINT,
    GUI_EVENT_DRAW_ENTER_HINT,

    GUI_EVENT_DRAW_FOCUS_BIT = 0x80U,
};

enum ProcessEvent {
    PROCESS_EVENT_RENDER_EFFECTS,
    PROCESS_EVENT_PRCESS_HOST_PACKET,
};

static void rendererProcess(unsigned int frameTime);
static void loadSettings(void);

static const Point2d SizHalfLine = {64, 12};
static const Point2d SizFullLine = {128, 12};
static const Point2d SizFiveChars = {38, 12};

static const Point2d PosRenderIndex = {GLOBAL_X_OFFSET, 2};
static const Point2d PosRenderMode = {GLOBAL_X_OFFSET + 64, 2};
static const Point2d PosEffectIndex = {GLOBAL_X_OFFSET, 14};
static const Point2d PosEffectType = {GLOBAL_X_OFFSET, 26};
static const Point2d PosPaletteIndex = {GLOBAL_X_OFFSET, 38};
static const Point2d PosPalette = {GLOBAL_X_OFFSET, 50};
static const Point2d PosEffectDuration = {GLOBAL_X_OFFSET, 62};
static const Point2d PosEffectLateral = {GLOBAL_X_OFFSET, 74};
static const Point2d PosEffectReverse = {GLOBAL_X_OFFSET + 38, 74};
static const Point2d PosEffectMirror = {GLOBAL_X_OFFSET + 76, 74};
static const Point2d PosViewportIndex = {GLOBAL_X_OFFSET, 86};
static const Point2d PosViewportOffset = {GLOBAL_X_OFFSET, 98};
static const Point2d PosViewportSize = {GLOBAL_X_OFFSET + 64, 98};

static const Point2d PosDialHint = {GLOBAL_X_OFFSET, 116};
static const Point2d PosEnterHint = {GLOBAL_X_OFFSET + 64, 116};

enum RendererMode {
    RendererModeDisabled,   // will fill viewport with zeroes
    RendererModeBypass,     // viewport will be updated constantly
    RendererModeGated,      // viewport will be updated with effect if gate on, otherwise zeroed
    RendererModeInvGated,   // viewport will be updated with effect if gate off, otherise zeroed
    RendererModeCount
};

static const uint32_t framePeriodMs           = 25;

static const char * RendererModeStrings[RendererModeCount] = {
    [RendererModeDisabled] = "Off",
    [RendererModeBypass] = "Bypass",
    [RendererModeGated] = "Gated",
    [RendererModeInvGated] = "N-Gated",
};
static const char * EffectTypeStrings[EffectTypeCount] = {
    [EffectTypeNone]    = "None",
    [EffectTypeFill]    = "Fill",
    [EffectTypeSwitch]  = "Switch",
    [EffectTypeFade]    = "Fade",
    [EffectTypeChase]   = "Chase",
    [EffectTypeRainbow] = "Rainbow",
};

static enum SettingsIndex {
    SettingsRendererIndex,
    SettingsRendererMode,
    SettingsEffectIndex,
    SettingsEffectType,
    SettingsPaletteIndex,
    SettingsColorIndex,
    SettingsEffectDuration,
    SettingsEffectLateral,
    SettingsEffectReverse,
    SettingsEffectMirror,
    SettingsViewportIndex,
    SettingsIndexCount
} settingsIndex = SettingsRendererIndex;
static enum DialHintState {
    DIAL_HINT_STATE_GATE,
    DIAL_HINT_STATE_SAVE,
} dialHintState = DIAL_HINT_STATE_GATE;
static enum EnterHintState {
    ENTER_HINT_STATE_NONE,
    ENTER_HINT_STATE_REVERT
} enterHintState = ENTER_HINT_STATE_NONE;
static uint32_t rendererIndex = 0;
static RendererSettings rendererSettingsSnapshot;
static EffectSettings effectSettingsSnapshot;
static uint8_t viewportPool[VIEWPORT_POOL_SIZE];
static bool gateState = false;

static void channelOutput(EffectColor * color, unsigned int pointOffset, void * userData)
{
    uint32_t vpOffset = *(uint32_t *)userData;
    uint32_t offset = vpOffset + pointOffset * 3;
    if (offset + 2 > sizeof(viewportPool))
        return;
    viewportPool[offset] = color->r;
    viewportPool[offset + 1] = color->g;
    viewportPool[offset + 2] = color->b;
}

static void rendererProcess(unsigned int frameTime)
{
    memset(viewportPool, 0, VIEWPORT_POOL_SIZE);
    for (uint32_t i = 0; i < RENDERER_COUNT; i++) {
        RendererSettings renderer;
        if (i == rendererIndex) {
            // get settings from temporary/changeable state
            renderer = rendererSettingsSnapshot;
        } else {
            settingsGetRenderer(i, &renderer);
        }

        // skip renderer if it is disabled
        // if gated mode does not complement gate state
        if (renderer.mode == RendererModeDisabled
            || (renderer.mode == RendererModeGated && !gateState)
            || (renderer.mode == RendererModeInvGated && gateState)) {
            continue;
        }

        ViewportSettings viewport;
        settingsGetViewport(renderer.viewport, &viewport);
        if (viewport.size == 0 
            || viewport.size + viewport.offset > sizeof(viewportPool)) {
            continue;
        }
        uint32_t pointCount = viewport.size / 3;

        EffectSettings effect;
        if (renderer.effect == rendererSettingsSnapshot.effect) {
            // get settings from temporary/changeable state
            effect = effectSettingsSnapshot;
        } else {
            settingsGetEffect(renderer.effect, &effect);
        }
        if (effect.type == EffectTypeNone) {
            continue;
        }

        Palette palette;
        settingsGetPalette(effect.palette, &palette);
        if (palette.colorCount == 0) {
            continue;
        }

        if (effect.type == EffectTypeFill) {
            RgbColor rgb = palette.color[effect.colorIndex];
            EffectColor color;
            color.r = rgb.r;
            color.g = rgb.g;
            color.b = rgb.b;
            for (uint32_t point = 0; point < pointCount; point++) {
                channelOutput(&color, point, &viewport.offset);
            }
            continue;
        }

        SwitchEffectSettings switchFx;
        FadeEffectSettings fadeFx;
        ChaseEffectSettings chaseFx;
        RainbowEffectSettings rainbowFx;
        EffectState state = {
            .outputCallback = channelOutput,
            .timestamp      = frameTime,
            .pointCount     = pointCount,
            .userData       = &viewport.offset
        };

        switch (effect.type) {
        case EffectTypeSwitch:
            switchFx.duration   = effect.durationMs;
            switchFx.lateral    = effect.lateral ? 1 : 0;
            switchFx.reverse    = effect.reverse ? 1 : 0;
            switchFx.mirror     = effect.mirror  ? 1 : 0;
            switchFx.colors     = (EffectColor *)palette.color;
            switchFx.colorCount = palette.colorCount;
            effectSwitch(&state, &switchFx);
            break;
        case EffectTypeFade:
            fadeFx.duration   = effect.durationMs;
            fadeFx.lateral    = effect.lateral ? 1 : 0;
            fadeFx.reverse    = effect.reverse ? 1 : 0;
            fadeFx.mirror     = effect.mirror  ? 1 : 0;
            fadeFx.colors     = (EffectColor *)palette.color;
            fadeFx.colorCount = palette.colorCount;
            effectFade(&state, &fadeFx);
            break;
        case EffectTypeChase:
            chaseFx.duration   = effect.durationMs;
            chaseFx.lateral    = effect.lateral ? 1 : 0;
            chaseFx.reverse    = effect.reverse ? 1 : 0;
            chaseFx.mirror     = effect.mirror  ? 1 : 0;
            chaseFx.colors     = (EffectColor *)palette.color;
            chaseFx.colorCount = palette.colorCount;
            effectChase(&state, &chaseFx);
            break;
        case EffectTypeRainbow:
            rainbowFx.lateral  = effect.lateral ? 1 : 0;
            rainbowFx.duration = effect.durationMs;
            effectRainbow(&state, &rainbowFx);
            break;
        default:
            break;
        }
    }
}

static bool isConfigChanged(void)
{
    RendererSettings rendererSettings;
    settingsGetRenderer(rendererIndex, &rendererSettings);
    bool rendererChanged = 0 != memcmp(&rendererSettingsSnapshot,
                                       &rendererSettings,
                                       sizeof(rendererSettings));
    EffectSettings effectSettings;
    settingsGetEffect(rendererSettingsSnapshot.effect, &effectSettings);
    bool effectChanged = 0 != memcmp(&effectSettingsSnapshot,
                                     &effectSettings,
                                     sizeof(effectSettings));
    return rendererChanged || effectChanged; 
}

static void queueSettingsControlDraw(enum SettingsIndex index, bool focus)
{
    uint32_t event = 0;
    uint32_t arg = 0;
    switch (index) {
    case SettingsRendererIndex:
        event = GUI_EVENT_DRAW_RENDERER_INDEX;
        break;
    case SettingsRendererMode:
        event = GUI_EVENT_DRAW_RENDER_MODE;
        break;
    case SettingsEffectIndex:
        event = GUI_EVENT_DRAW_EFFECT_INDEX;
        break;
    case SettingsEffectType:
        event = GUI_EVENT_DRAW_EFFECT_TYPE;
        break;
    case SettingsPaletteIndex:
        event = GUI_EVENT_DRAW_PALETTE_INDEX;
        break;
    case SettingsColorIndex:
        event = GUI_EVENT_DRAW_PALETTE;
        break;
    case SettingsEffectDuration:
        event = GUI_EVENT_DRAW_EFFECT_DURATION;
        break;
    case SettingsEffectLateral:
        event = GUI_EVENT_DRAW_EFFECT_LATERAL;
        break;
    case SettingsEffectReverse:
        event = GUI_EVENT_DRAW_EFFECT_REVERSE;
        break;
    case SettingsEffectMirror:
        event = GUI_EVENT_DRAW_EFFECT_MIRROR;
        break;
    case SettingsViewportIndex:
        event = GUI_EVENT_DRAW_VIEWPORT_INDEX;
        break;
    default:
        return;
    }
    if (focus) {
        event |= GUI_EVENT_DRAW_FOCUS_BIT;
    }
    appletManagerQueueGuiEvent(event, arg);
}

static void queueEffectProcess(void)
{
    appletManagerQueueProcessEvent(PROCESS_EVENT_RENDER_EFFECTS, 0);
}

static void appletOnEnter(void)
{
    settingsIndex = SettingsRendererIndex;
    enterHintState = ENTER_HINT_STATE_NONE;
    dialHintState = DIAL_HINT_STATE_GATE;
    gateState = false;
    settingsGetRenderer(rendererIndex, &rendererSettingsSnapshot);
    settingsGetEffect(rendererSettingsSnapshot.effect, &effectSettingsSnapshot);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
    queueSettingsControlDraw(settingsIndex, true);
    appletManagerStartTimer(framePeriodMs, queueEffectProcess);
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2)
{
    static uint32_t timestamp = 0;
    switch ((enum ProcessEvent)arg1) {
    case PROCESS_EVENT_RENDER_EFFECTS:
        appletManagerStartTimer(framePeriodMs, queueEffectProcess);
        rendererProcess(timestamp);
        displayManagerUpdateChannelData(viewportPool, sizeof(viewportPool));
        timestamp += framePeriodMs;
        break;
    case PROCESS_EVENT_PRCESS_HOST_PACKET:
        break;
    }
}

static void appletOnGui(uint32_t arg1, uint32_t arg2)
{
    bool inFocus = (arg1 & GUI_EVENT_DRAW_FOCUS_BIT) != 0;
    arg1 = arg1 & (~GUI_EVENT_DRAW_FOCUS_BIT);
    bool drawAll = false;
    ColorPickerSettings colorPicker;
    Palette palette;
    ViewportSettings viewportSettings;
    RendererSettings *renderer = &rendererSettingsSnapshot;
    EffectSettings *effect = &effectSettingsSnapshot;

    switch ((enum GuiEvent)arg1) {

    case GUI_EVENT_DRAW_ALL:
        inFocus = false;
        drawAll = true;
        // fallthrough

    case GUI_EVENT_CLEAR_SCREEN:
        guiElementFillBackground();
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_RENDERER_INDEX:
        guiElementLabeledNumericValue(&rendererIndex, NumericValueTypeU32, "Render:", 
                                      inFocus, NumericBaseDec, &PosRenderIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_RENDER_MODE:
        guiElementLabel(RendererModeStrings[renderer->mode], inFocus, 
                        &PosRenderMode, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_INDEX:
        guiElementLabeledNumericValue(&renderer->effect, NumericValueTypeU32, "Effect:", 
                                      inFocus, NumericBaseDec, &PosEffectIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_TYPE:
        guiElementLabeledString("Type:", EffectTypeStrings[effect->type], 
                                inFocus, &PosEffectType, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_PALETTE_INDEX:
        guiElementLabeledNumericValue(&effect->palette, NumericValueTypeU32, "Palette:", 
                                      inFocus, NumericBaseDec, &PosPaletteIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_PALETTE:
        settingsGetPalette(effect->palette, &palette);
        colorPicker.isHsv = false;
        colorPicker.position = &PosPalette;
        colorPicker.size = &SizFullLine;
        colorPicker.colorCount = palette.colorCount;
        colorPicker.rgbColors = palette.color;
        colorPicker.colorElementHeight = 11;
        colorPicker.colorElementWidth  = 12;
        colorPicker.inFocus = inFocus,
        colorPicker.colorIndex = effect->colorIndex;
        guiElementColorPicker(&colorPicker);
        if (!drawAll) {
            break;
        }
        
    case GUI_EVENT_DRAW_EFFECT_DURATION:
        guiElementLabeledNumericValue(&effect->durationMs, NumericValueTypeU32, "Duration:", 
                                      inFocus, NumericBaseDec, &PosEffectDuration, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_LATERAL:
        guiElementCheckBox("LA", effect->lateral, 
                           inFocus, &PosEffectLateral, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_REVERSE:
        guiElementCheckBox("RE", effect->reverse, 
                           inFocus, &PosEffectReverse, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_MIRROR:
        guiElementCheckBox("MI", effect->mirror, 
                           inFocus, &PosEffectMirror, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_VIEWPORT_INDEX:
        settingsGetViewport(renderer->viewport, &viewportSettings);
        guiElementLabeledNumericValue(&renderer->viewport, NumericValueTypeU32, "Viewport", 
                                      inFocus, NumericBaseDec, &PosViewportIndex, &SizHalfLine);
        guiElementLabeledNumericValue(&viewportSettings.offset, NumericValueTypeU32, "@", 
                                      false, NumericBaseDec, &PosViewportOffset, &SizHalfLine);
        guiElementLabeledNumericValue(&viewportSettings.size, NumericValueTypeU32, ":", 
                                      false, NumericBaseDec, &PosViewportSize, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DIAL_HINT:
        switch (dialHintState) {
        case DIAL_HINT_STATE_SAVE:
            guiElementLabel("o   Save|", false, &PosDialHint, NULL);
            break;
        case DIAL_HINT_STATE_GATE:
            guiElementLabel("o   Gate|", false, &PosDialHint, NULL);
            break;
        }
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_ENTER_HINT:
        switch (enterHintState) {
        case ENTER_HINT_STATE_NONE:
            guiElementLabel("K2   Exit", false, &PosEnterHint, NULL);
            break;
        case ENTER_HINT_STATE_REVERT:
            guiElementLabel("K2 Revert", false, &PosEnterHint, NULL);
            break;
        }
        // fallthrough ends here
        break;

    case GUI_EVENT_DRAW_EFFECT_SETTINGS:
        appletOnGui(GUI_EVENT_DRAW_EFFECT_TYPE, 0);
        appletOnGui(GUI_EVENT_DRAW_PALETTE_INDEX, 0);
        appletOnGui(GUI_EVENT_DRAW_PALETTE, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_DURATION, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_LATERAL, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_REVERSE, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_MIRROR, 0);
        break;

    default:
        break;
    }
}

static void onButtonDown(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    if (settingsIndex + 1 < SettingsIndexCount) {
        settingsIndex++;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonUp(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    if (((int32_t)settingsIndex) - 1 >= 0) {
        settingsIndex--;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonDial(bool state)
{
    switch (dialHintState) {
    case DIAL_HINT_STATE_SAVE:
        if (state) {
            dialHintState = DIAL_HINT_STATE_GATE;
            enterHintState = ENTER_HINT_STATE_NONE;
            settingsSetRenderer(rendererIndex, &rendererSettingsSnapshot);
            settingsSetEffect(rendererSettingsSnapshot.effect, &effectSettingsSnapshot);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        }
        break;
    case DIAL_HINT_STATE_GATE:
        gateState = state;
        break;
    }
}

static void onButtonEnter(void)
{
    switch (enterHintState) {
    case ENTER_HINT_STATE_REVERT:
        // revert settings
        enterHintState = ENTER_HINT_STATE_NONE;
        dialHintState = DIAL_HINT_STATE_GATE;
        settingsGetEffect(rendererSettingsSnapshot.effect, &effectSettingsSnapshot);
        settingsGetRenderer(rendererIndex, &rendererSettingsSnapshot);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        queueSettingsControlDraw(settingsIndex, true);
        break;
    case ENTER_HINT_STATE_NONE:
        appletManagerQueueAppletExit();
        break;
    }
}

static void onEncoder(int32_t delta)
{
    bool recheckConfig = false;
    RendererSettings *renderer = &rendererSettingsSnapshot;
    EffectSettings *effect = &effectSettingsSnapshot;
    Palette palette;

    switch (settingsIndex) {
    case SettingsRendererIndex:
        if ((int32_t)rendererIndex + delta < RENDERER_COUNT
            && (int32_t)rendererIndex + delta >= 0) {
            rendererIndex += delta;
            enterHintState = ENTER_HINT_STATE_NONE;
            dialHintState = DIAL_HINT_STATE_GATE;
            settingsGetRenderer(rendererIndex, renderer);
            settingsGetEffect(renderer->effect, effect);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        }
        break;
    case SettingsRendererMode:
        if ((int32_t)renderer->mode + delta < RendererModeCount
            && (int32_t)renderer->mode + delta >= 0) {
            renderer->mode += delta;
            recheckConfig = true;
        }
        break;
    case SettingsEffectIndex:
        if ((int32_t)renderer->effect + delta < EFFECT_COUNT
            && ((int32_t)renderer->effect) + delta >= 0) {
            renderer->effect += delta;
            settingsGetEffect(renderer->effect, effect);
            recheckConfig = true;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_EFFECT_SETTINGS, 0);
        } 
        break;
    case SettingsEffectType:
        if ((int32_t)effect->type + delta < EffectTypeCount
            && (int32_t)effect->type + delta >= 0) {
            effect->type += delta;
            recheckConfig = true;
        }
        break;
    case SettingsPaletteIndex:
        if ((int32_t)effect->palette + delta < PALETTE_COUNT
            && (int32_t)effect->palette + delta >= 0) {
            effect->palette += delta;
            effect->colorIndex = 0;
            recheckConfig = true;
            queueSettingsControlDraw(SettingsColorIndex, false);
        }
        break;
    case SettingsColorIndex:
        settingsGetPalette(effect->palette, &palette);
        if ((int32_t)effect->colorIndex + delta < palette.colorCount
            && (int32_t)effect->colorIndex + delta >= 0) {
            effect->colorIndex += delta;
            recheckConfig = true;
        }
        break;
    case SettingsEffectDuration:
        if ((int32_t)effect->durationMs + delta >= 0) {
            effect->durationMs += delta * 100;
            recheckConfig = true;
        }
        break;
    case SettingsEffectLateral:
        if (delta > 0) {
            effect->lateral = true;
        } else if (delta < 0) {
            effect->lateral = false;
        }
        recheckConfig = true;
        break;
    case SettingsEffectReverse:
        if (delta > 0) {
            effect->reverse = true;
        } else if (delta < 0) {
            effect->reverse = false;
        }
        recheckConfig = true;
        break;
    case SettingsEffectMirror:
        if (delta > 0) {
            effect->mirror = true;
        } else if (delta < 0) {
            effect->mirror = false;
        }
        recheckConfig = true;
        break;
    case SettingsViewportIndex:
        if ((int32_t)renderer->viewport + delta < VIEWPORT_COUNT
            && (int32_t)renderer->viewport + delta >= 0) {
            renderer->viewport += delta;
            recheckConfig = true;
        }
        break;
    default:
        break;
    }

    if (recheckConfig) {
        queueSettingsControlDraw(settingsIndex, true);
        if (isConfigChanged()) {
            dialHintState = DIAL_HINT_STATE_SAVE;
            enterHintState = ENTER_HINT_STATE_REVERT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        } else {
            if (dialHintState != DIAL_HINT_STATE_GATE) {
                dialHintState = DIAL_HINT_STATE_GATE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
            if (enterHintState != ENTER_HINT_STATE_NONE) {
                enterHintState = ENTER_HINT_STATE_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            }
        }
    }
}

static void appletOnInput(InputEvent evt)
{
    switch (evt.type) {
    case EventButtonDown:
        if (evt.buttonState) {
            onButtonDown();
        } 
        break;
    case EventButtonUp:
        if (evt.buttonState) {
            onButtonUp();
        }
        break;
    case EventButtonDial:
        onButtonDial(evt.buttonState);
        break;
    case EventButtonEnter:
        if (evt.buttonState) {
            onButtonEnter();
        }
        break;
    case EventEncoder:
        onEncoder(evt.encoderDelta);
        break;
    }
}

static void appletOnHostDataEvent(uint8_t *data, size_t size)
{

}

static void appletOnExit(void)
{

}

void rendererControlStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}