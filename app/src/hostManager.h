#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE (2048U)

typedef enum {
    HOST_MANAGER_CHANNEL_PROTOCOL_GENERIC_SERIAL,
    HOST_MANAGER_CHANNEL_PROTOCOL_RENARD,
    HOST_MANAGER_CHANNEL_PROTOCOL_COUNT
} HostManagerChannelProtocol;

typedef void (*HostManagerChannelDataReceivedCallback) (uint8_t *data, size_t len);
typedef void (*HostManagerControlDataReceivedCallback) (uint8_t *data, size_t len);
typedef void (*HostManagerConnectionStateCallback) (bool connected);

void hostManagerInit(HostManagerChannelDataReceivedCallback chanDataRx, 
                     HostManagerControlDataReceivedCallback ctlDataRx,
                     HostManagerConnectionStateCallback connectionCb,
                     HostManagerChannelProtocol channelProto);
void hostManagerControlDataSend(uint8_t *data, size_t len);