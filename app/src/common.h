#pragma once
#include <stdint.h>

#define ELEMENTS(x)     ( sizeof(x) / sizeof(*x) )
#define member_size(type, member) sizeof(((type *)0)->member)
#define NORETURN        __attribute__((noreturn))

typedef struct Point2d {
    uint32_t x;
    uint32_t y;
} Point2d;
