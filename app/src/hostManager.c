#include "hostManager.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_hid_generic_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"

static void cdcAcmEvtHandler(app_usbd_class_inst_t const * p_inst,
                             app_usbd_cdc_acm_user_event_t event);
static void usbdUserEvtHandler(app_usbd_event_type_t event);

#define CDC_ACM_COMM_INTERFACE  0
#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE  1
#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1

#define RENARD_BYTE_PAD (0x7DU)
#define RENARD_BYTE_SYNC (0x7EU)
#define RENARD_BYTE_ESCAPE (0x7FU)
#define RENARD_BYTE_COMMAND (0x80U)
#define RENARD_BYTE_ESCAPE_0x7D (0x2FU)
#define RENARD_BYTE_ESCAPE_0x7E (0x30U)
#define RENARD_BYTE_ESCAPE_0x7F (0x31U)

static HostManagerChannelDataReceivedCallback channelDataReceivedCb = NULL;
static HostManagerControlDataReceivedCallback controlDataReceivedCb = NULL;
static HostManagerConnectionStateCallback connectionStateCb = NULL;
static uint8_t dataRxBuffer = 0;
static HostManagerChannelProtocol channelProtocol = HOST_MANAGER_CHANNEL_PROTOCOL_GENERIC_SERIAL;
static uint8_t channelDataBuffer[HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE] = {0};
static size_t channelDataBufferOffset = 0;

static enum {
    RENARD_STATE_WAIT_SYNC,
    RENARD_STATE_WAIT_COMMAND,
    RENARD_STATE_ESCAPE_RECEIVED,
    RENARD_STATE_WAIT_DATA,
    RENARD_STATE_WAIT_ESCAPED_DATA,
} renardState = RENARD_STATE_WAIT_SYNC;

APP_USBD_CDC_ACM_GLOBAL_DEF(cdcAcmInstance,
                            cdcAcmEvtHandler,
                            CDC_ACM_COMM_INTERFACE,
                            CDC_ACM_DATA_INTERFACE,
                            CDC_ACM_COMM_EPIN,
                            CDC_ACM_DATA_EPIN,
                            CDC_ACM_DATA_EPOUT,
                            APP_USBD_CDC_COMM_PROTOCOL_AT_V250
);

static void processChannelDataRenard(uint8_t byte)
{
    switch (byte) {
    case RENARD_BYTE_SYNC:
        // previous data must be flushed now, start of new packet
        if (channelDataBufferOffset > 0 && channelDataReceivedCb != NULL) {
            channelDataReceivedCb(channelDataBuffer, channelDataBufferOffset);
        }
        channelDataBufferOffset = 0;
        renardState = RENARD_STATE_WAIT_COMMAND;
        break;
    case RENARD_BYTE_PAD:
        // presumably no meaning for now
        break;
    case RENARD_BYTE_COMMAND:
        // next bytes until sync are channel data
        renardState = RENARD_STATE_WAIT_DATA;
        break;
    case RENARD_BYTE_ESCAPE:
        // content of next byte is escaped as channel data
        // from service byte values
        renardState = RENARD_STATE_WAIT_ESCAPED_DATA;
        break;
    case RENARD_BYTE_ESCAPE_0x7D:
    case RENARD_BYTE_ESCAPE_0x7E:
    case RENARD_BYTE_ESCAPE_0x7F:
        if (renardState == RENARD_STATE_WAIT_ESCAPED_DATA) {
            byte = 0x7D + (byte - RENARD_BYTE_ESCAPE_0x7D);
            renardState = RENARD_STATE_WAIT_DATA;
        }
    default:
        if (channelDataBufferOffset >= sizeof(channelDataBuffer)) {
            // overflow!
            break;
        }

        channelDataBuffer[channelDataBufferOffset++] = byte;
        break;
    }
}

static void processChannelData(uint8_t byte)
{
    switch (channelProtocol) {
    case HOST_MANAGER_CHANNEL_PROTOCOL_RENARD:
        processChannelDataRenard(byte);
        break;
    default:
        assert(0);
        break;
    }
}

static void cdcAcmEvtHandler(app_usbd_class_inst_t const * p_inst,
                             app_usbd_cdc_acm_user_event_t event)
{
    app_usbd_cdc_acm_t const *cdcAcm = app_usbd_cdc_acm_class_get(p_inst);

    switch (event) {
    case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN: {
        /*Setup first transfer*/
        ret_code_t ret = app_usbd_cdc_acm_read(&cdcAcmInstance,
                                                &dataRxBuffer,
                                                1);
        UNUSED_VARIABLE(ret);
        if (connectionStateCb != NULL) {
            connectionStateCb(true);
        }
    } break;
    case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
        if (connectionStateCb != NULL) {
            connectionStateCb(false);
        }
        break;
    case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
        break;
    case APP_USBD_CDC_ACM_USER_EVT_RX_DONE: {
        ret_code_t ret;
        //NRF_LOG_INFO("Bytes waiting: %d", app_usbd_cdc_acm_bytes_stored(cdcAcm));
        processChannelData(dataRxBuffer);
        do
        {
            /*Get amount of data transfered*/
            size_t size = app_usbd_cdc_acm_rx_size(cdcAcm);
            //NRF_LOG_INFO("RX: size: %lu char: %c", size, cdcDataRxBuffer[0]);

            /* Fetch data until internal buffer is empty */
            ret = app_usbd_cdc_acm_read(&cdcAcmInstance,
                                        &dataRxBuffer,
                                        1);
            if (ret == NRF_SUCCESS) {
                processChannelData(dataRxBuffer);
            }
        } while (ret == NRF_SUCCESS);
    } break;
    default:
        break;
    }
}

static void usbdUserEvtHandler(app_usbd_event_type_t event)
{
    switch (event)
    {
    case APP_USBD_EVT_DRV_SUSPEND:
        break;
    case APP_USBD_EVT_DRV_RESUME:
        break;
    case APP_USBD_EVT_STARTED:
        break;
    case APP_USBD_EVT_STOPPED:
        app_usbd_disable();
        break;
    case APP_USBD_EVT_POWER_DETECTED:
        //NRF_LOG_INFO("USB power detected");

        if (!nrf_drv_usbd_is_enabled())
        {
            app_usbd_enable();
        }
        break;
    case APP_USBD_EVT_POWER_REMOVED:
        //NRF_LOG_INFO("USB power removed");
        app_usbd_stop();
        break;
    case APP_USBD_EVT_POWER_READY:
        //NRF_LOG_INFO("USB ready");
        app_usbd_start();
        break;
    default:
        break;
    }
}

static bool initUsb(void)
{
    // need lfclk running prior
    ret_code_t ret;

    static const app_usbd_config_t usbdConfig = {
        .ev_state_proc = usbdUserEvtHandler,
        // .ev_isr_handler = usbdIsrEventHandler,
    };

    app_usbd_serial_num_generate();
    ret = app_usbd_init(&usbdConfig);
    APP_ERROR_CHECK(ret);

    app_usbd_class_inst_t const *cdcAcmClass = app_usbd_cdc_acm_class_inst_get(&cdcAcmInstance);
    ret = app_usbd_class_append(cdcAcmClass);
    APP_ERROR_CHECK(ret);

    ret = app_usbd_power_events_enable();
    APP_ERROR_CHECK(ret);

    return ret == NRF_SUCCESS;
}

void hostManagerInit(HostManagerChannelDataReceivedCallback chanDataRx, 
                     HostManagerControlDataReceivedCallback ctlDataRx,
                     HostManagerConnectionStateCallback connectionCb,
                     HostManagerChannelProtocol channelProto)
{
    channelDataReceivedCb = chanDataRx;
    controlDataReceivedCb = ctlDataRx;
    connectionStateCb = connectionCb;
    channelProtocol = channelProto;
    renardState = RENARD_STATE_WAIT_SYNC;
    channelDataBufferOffset = 0;
    initUsb();
}

void hostManagerControlDataSend(uint8_t *data, size_t len)
{

}