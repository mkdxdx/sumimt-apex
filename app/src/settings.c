#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "settings.h"
#include "color.h"
#include "board.h"

static void save(void);

static uint8_t settingsBuffer[BOARD_SECTOR_SIZE] __attribute__((section(".settings_page"))) __attribute__((used));
static union {
    SystemSettings settings;
    uint8_t data[BOARD_SECTOR_SIZE];
} rwSettings;

extern SystemSettings defSettingsValues;

static SettingsLockCallback lock = NULL;

void settingsInit(SettingsLockCallback lockCb)
{
    lock = lockCb;
    memcpy(rwSettings.data, settingsBuffer, sizeof(rwSettings.data));
}

void settingsGetPalette(uint32_t index, Palette * settings)
{
    *settings = rwSettings.settings.palette[index];
}

void settingsGetRenderer(uint32_t index, RendererSettings * settings)
{
    *settings = rwSettings.settings.renderer[index];
}

void settingsGetViewport(uint32_t index, ViewportSettings * settings)
{
    *settings = rwSettings.settings.viewport[index];
}

void settingsGetDisplay(uint32_t index, DisplaySettings * settings)
{
    *settings = rwSettings.settings.display[index];
}

void settingsGetEffect(uint32_t index, EffectSettings * settings)
{
    *settings = rwSettings.settings.effect[index];
}

void settingsSetPalette(uint32_t index, Palette * settings)
{
    if (lock)
        lock(true);
    rwSettings.settings.palette[index] = *settings;
    save();
    if (lock)
        lock(false);
}

void settingsSetRenderer(uint32_t index, RendererSettings * settings)
{
    if (lock)
        lock(true);
    rwSettings.settings.renderer[index] = *settings;
    save();
    if (lock)
        lock(false);
}

void settingsSetViewport(uint32_t index, ViewportSettings * settings)
{
    if (lock)
        lock(true);
    rwSettings.settings.viewport[index] = *settings;
    save();
    if (lock)
        lock(false);
}

void settingsSetDisplay(uint32_t index, DisplaySettings * settings)
{
    if (lock)
        lock(true);
    rwSettings.settings.display[index] = *settings;
    save();
    if (lock)
        lock(false);
}

void settingsSetEffect(uint32_t index, EffectSettings * settings)
{
    if (lock)
        lock(true);
    rwSettings.settings.effect[index] = *settings;
    save();
    if (lock)
        lock(false);
}

void settingsSetDefault(void)
{
    if (lock)
        lock(true);
    rwSettings.settings = defSettingsValues;
    save();
    if (lock)
        lock(false);
}

static void save(void)
{
    boardFlashPageWrite((uint32_t)settingsBuffer, rwSettings.data);
}
