#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "rendererTypes.h"

/*
#define MAX_BRUSHES                   5
#define MAX_PALETTES                  9
#define MAX_PALETTE_COLOR_COUNT       8
#define EFFECT_SETTINGS_COUNT_NONE    0
#define EFFECT_SETTINGS_COUNT_FILL    2
#define EFFECT_SETTINGS_COUNT_SWITCH  5
#define EFFECT_SETTINGS_COUNT_FADE    5
#define EFFECT_SETTINGS_COUNT_CHASE   5
#define EFFECT_SETTINGS_COUNT_RAINBOW 2
*/


typedef struct LimitPair {
    /* uint16_t minimum; */
    uint16_t maximum;
} LimitPair;

typedef struct LimitList {
    uint32_t pairCount;
    LimitPair * limitPairs;
} LimitList;

/*

typedef struct EffectSettingsFill {
    uint32_t paletteIndex;
    uint32_t colorIndex;
} EffectSettingsFill;

typedef struct EffectSettingsSwitch {
    uint32_t paletteIndex;
    uint32_t durationMsX100;
    bool     lateral;
    bool     reverse;
    bool     mirror;
} EffectSettingsSwitch;

typedef struct EffectSettingsFade {
    uint32_t paletteIndex;
    uint32_t durationMsX100;
    bool     lateral;
    bool     reverse;
    bool     mirror;
} EffectSettingsFade;

typedef struct EffectSettingsChase {
    uint32_t paletteIndex;
    uint32_t durationMsX100;
    bool     lateral;
    bool     reverse;
    bool     mirror;
} EffectSettingsChase;

typedef struct EffectSettingsRainbow {
    uint32_t durationMsX100;
    bool     lateral;
} EffectSettingsRainbow;

typedef union EffectParameters {
    EffectSettingsFill    settingsFill;
    EffectSettingsSwitch  settingsSwitch;
    EffectSettingsFade    settingsFade;
    EffectSettingsChase   settingsChase;
    EffectSettingsRainbow settingsRainbow;
} EffectParameters;

typedef struct BrushEffectSettings {
    LightBrushParametricEffectType effectType;
    EffectParameters effectParameters;
} BrushEffectSettings;

typedef struct BrushSettings {
    bool remoteFlushEnabled;
    BrushEffectSettings effectSettings;
} BrushSettings;

*/
