#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


typedef struct {
    enum {
        EventButtonDial,
        EventButtonDown,
        EventButtonEnter,
        EventButtonUp,
        EventEncoder
    } type;
    union {
        bool buttonState;
        int32_t encoderDelta;
    };
} InputEvent;

typedef enum {
    APPLET_HOST_DATA_TYPE_CHANNEL,
    APPLET_HOST_DATA_TYPE_CONFIG,
    APPLET_HOST_DATA_TYPE_COUNT
} AppletHostDataType;

typedef void (*AppletOnEnterEvent)(void);
typedef void (*AppletOnProcessEvent)(uint32_t arg1, uint32_t arg2);
typedef void (*AppletOnGuiEvent)(uint32_t arg1, uint32_t arg2);
typedef void (*AppletOnInputEvent)(InputEvent event);
typedef void (*AppletOnHostDataEvent)(AppletHostDataType type, 
                                      const uint8_t *data, size_t len);
typedef void (*AppletOnExitEvent)(void);

typedef struct {
    AppletOnEnterEvent onEnter;
    AppletOnProcessEvent onProcess;
    AppletOnGuiEvent onGui;
    AppletOnInputEvent onInput;
    AppletOnHostDataEvent onHostDataEvent;
    AppletOnExitEvent onExit;
} AppletCallbacks;