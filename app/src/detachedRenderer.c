#include "detachedRenderer.h"
#include "displayMode.h"
#include "common.h"
#include "settings.h"
#include "guiElements.h"
#include "rendererCommon.h"
#include "displayManager.h"
#include "appletManager.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define GLOBAL_X_OFFSET (4U)

enum GuiEvent {
    GUI_EVENT_CLEAR_SCREEN,
    GUI_EVENT_DRAW_ALL,
    GUI_EVENT_DRAW_DISPLAY_INDEX,
    GUI_EVENT_DRAW_EFFECT_INDEX,
    GUI_EVENT_DRAW_EFFECT_SETTINGS,
    GUI_EVENT_DRAW_EFFECT_TYPE,
    GUI_EVENT_DRAW_PALETTE_INDEX,
    GUI_EVENT_DRAW_PALETTE,
    GUI_EVENT_DRAW_EFFECT_DURATION,
    GUI_EVENT_DRAW_EFFECT_LATERAL,
    GUI_EVENT_DRAW_EFFECT_REVERSE,
    GUI_EVENT_DRAW_EFFECT_MIRROR,
    GUI_EVENT_DRAW_DIAL_HINT,
    GUI_EVENT_DRAW_ENTER_HINT,
    GUI_EVENT_DRAW_COMM_ERR_HINT,

    GUI_EVENT_DRAW_FOCUS_BIT = 0x80U,
};

static const Point2d SizHalfLine = {64, 12};
static const Point2d SizFullLine = {128, 12};
static const Point2d SizFiveChars = {38, 12};

static const Point2d PosDisplayIndex = {GLOBAL_X_OFFSET, 2};
static const Point2d PosEffectIndex = {GLOBAL_X_OFFSET, 14};
static const Point2d PosEffectType = {GLOBAL_X_OFFSET, 26};
static const Point2d PosPaletteIndex = {GLOBAL_X_OFFSET, 38};
static const Point2d PosPalette = {GLOBAL_X_OFFSET, 50};
static const Point2d PosEffectDuration = {GLOBAL_X_OFFSET, 62};
static const Point2d PosEffectLateral = {GLOBAL_X_OFFSET, 74};
static const Point2d PosEffectReverse = {GLOBAL_X_OFFSET + 38, 74};
static const Point2d PosEffectMirror = {GLOBAL_X_OFFSET + 76, 74};

static const Point2d PosDialHint = {GLOBAL_X_OFFSET, 116};
static const Point2d PosEnterHint = {GLOBAL_X_OFFSET + 64, 116};
static const Point2d PosCommErrHint = {GLOBAL_X_OFFSET, 98};

static const char * EffectTypeStrings[EffectTypeCount] = {
    [EffectTypeNone]    = "None",
    [EffectTypeFill]    = "Fill",
    [EffectTypeSwitch]  = "Switch",
    [EffectTypeFade]    = "Fade",
    [EffectTypeChase]   = "Chase",
    [EffectTypeRainbow] = "Rainbow",
};

static enum SettingsIndex {
    SettingsDisplayIndex,
    SettingsEffectIndex,
    SettingsEffectType,
    SettingsEffectPaletteIndex,
    SettingsColorIndex,
    SettingsEffectDuration,
    SettingsEffectLateral,
    SettingsEffectReverse,
    SettingsEffectMirror,
    SettingsCount
} settingsIndex = SettingsDisplayIndex;
static enum {
    DIAL_HINT_STATE_SEND,
    DIAL_HINT_STATE_SAVE
} dialHintState = DIAL_HINT_STATE_SEND;
static enum {
    ENTER_HINT_STATE_NONE,
    ENTER_HINT_STATE_REVERT
} enterHintState = ENTER_HINT_STATE_NONE;
static DisplaySettings displaySettingsSnapshot;
static EffectSettings effectSettingsSnapshot;
static uint32_t displayIndex = 0;

static bool isConfigChanged(void)
{
    DisplaySettings displaySettings;
    settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
    bool displayChanged = 0 != memcmp(&displaySettingsSnapshot,
                                       &displaySettings,
                                       sizeof(displaySettings));
    EffectSettings effectSettings;
    settingsGetEffect(displaySettingsSnapshot.effect, &effectSettings);
    bool effectChanged = 0 != memcmp(&effectSettingsSnapshot,
                                     &effectSettings,
                                     sizeof(effectSettings));
    return displayChanged || effectChanged; 
}

static void queueSettingsControlDraw(enum SettingsIndex index, bool focus)
{
    uint32_t event = 0;
    uint32_t arg = 0;
    switch (index) {
    case SettingsDisplayIndex:
        event = GUI_EVENT_DRAW_DISPLAY_INDEX;
        break;
    case SettingsEffectIndex:
        event = GUI_EVENT_DRAW_EFFECT_INDEX;
        break;
    case SettingsEffectType:
        event = GUI_EVENT_DRAW_EFFECT_TYPE;
        break;
    case SettingsEffectPaletteIndex:
        event = GUI_EVENT_DRAW_PALETTE_INDEX;
        break;
    case SettingsColorIndex:
        event = GUI_EVENT_DRAW_PALETTE;
        break;
    case SettingsEffectDuration:
        event = GUI_EVENT_DRAW_EFFECT_DURATION;
        break;
    case SettingsEffectLateral:
        event = GUI_EVENT_DRAW_EFFECT_LATERAL;
        break;
    case SettingsEffectReverse:
        event = GUI_EVENT_DRAW_EFFECT_REVERSE;
        break;
    case SettingsEffectMirror:
        event = GUI_EVENT_DRAW_EFFECT_MIRROR;
        break;
    default:
        return;
    }
    if (focus) {
        event |= GUI_EVENT_DRAW_FOCUS_BIT;
    }
    appletManagerQueueGuiEvent(event, arg);
}

static bool sendSettings(void)
{
    static const DisplayStaticEffectType EffectMap[] = {
        [EffectTypeNone]    = DisplayStaticEffectTypeNone,
        [EffectTypeChase]   = DisplayStaticEffectTypeChase,
        [EffectTypeSwitch]  = DisplayStaticEffectTypeSwitch,
        [EffectTypeFade]    = DisplayStaticEffectTypeFade,
        [EffectTypeRainbow] = DisplayStaticEffectTypeRainbow,
        [EffectTypeFill]    = DisplayStaticEffectTypeFill,
    };
    DisplaySettings *display = &displaySettingsSnapshot;
    EffectSettings *effect = &effectSettingsSnapshot;
    DisplayConfig cfg;
    Palette palette;
    DisplayStaticEffect displayEffect;
    DisplayStaticEffectColor *displayPalette = NULL;
    settingsGetPalette(effect->palette, &palette);
    displayEffect.type = EffectMap[effect->type];
    displayEffect.reverse = effect->reverse > 0;
    displayEffect.lateral = effect->lateral > 0;
    displayEffect.mirror = effect->mirror > 0;
    displayEffect.periodMs = effect->durationMs;
    displayEffect.paletteColorCount = palette.colorCount;
    displayEffect.paletteColorIndex = effect->colorIndex;
    if (palette.colorCount > 0) {
        uint32_t paletteSize = sizeof(DisplayStaticEffectColor) * palette.colorCount;
        displayPalette = (DisplayStaticEffectColor *) appletManagerRequestMemory(paletteSize);
        memset(displayPalette, 0, paletteSize);
        for (uint32_t i = 0; i < palette.colorCount; i++) {
            displayPalette[i].r = palette.color[i].r;
            displayPalette[i].g = palette.color[i].g;
            displayPalette[i].b = palette.color[i].b;
        }

    }
    displayEffect.paletteColor = displayPalette;
    bool result = displayManagerSetStaticEffect(displayIndex, &displayEffect);
    if (displayPalette != NULL) {
        appletManagerFreeMemory(displayPalette);
    }
    return result;
}

static void appletOnEnter(void)
{
    displayIndex = 0;
    settingsIndex = SettingsDisplayIndex;
    dialHintState = DIAL_HINT_STATE_SEND;
    enterHintState = ENTER_HINT_STATE_NONE;
    settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
    settingsGetEffect(displaySettingsSnapshot.effect, &effectSettingsSnapshot);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
    queueSettingsControlDraw(settingsIndex, true);
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2)
{

}

static void appletOnGui(uint32_t arg1, uint32_t arg2) 
{
    bool inFocus = (arg1 & GUI_EVENT_DRAW_FOCUS_BIT) != 0;
    arg1 = arg1 & (~GUI_EVENT_DRAW_FOCUS_BIT);
    bool drawAll = false;
    ColorPickerSettings colorPicker;
    Palette palette;
    DisplaySettings *display = &displaySettingsSnapshot;
    EffectSettings *effect = &effectSettingsSnapshot;

    switch ((enum GuiEvent)arg1) {

    case GUI_EVENT_DRAW_ALL:
        inFocus = false;
        drawAll = true;
        // fallthrough

    case GUI_EVENT_CLEAR_SCREEN:
        guiElementFillBackground();
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DISPLAY_INDEX:
        guiElementLabeledNumericValue(&displayIndex, NumericValueTypeU32, "Display:", 
                                      inFocus, NumericBaseDec, &PosDisplayIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_INDEX:
        guiElementLabeledNumericValue(&display->effect, NumericValueTypeU32, "Effect:", 
                                      inFocus, NumericBaseDec, &PosEffectIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_TYPE:
        guiElementLabeledString("Type:", EffectTypeStrings[effect->type], 
                                inFocus, &PosEffectType, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_PALETTE_INDEX:
        guiElementLabeledNumericValue(&effect->palette, NumericValueTypeU32, "Palette:", 
                                      inFocus, NumericBaseDec, &PosPaletteIndex, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_PALETTE:
        settingsGetPalette(effect->palette, &palette);
        colorPicker.isHsv = false;
        colorPicker.position = &PosPalette;
        colorPicker.size = &SizFullLine;
        colorPicker.colorCount = palette.colorCount;
        colorPicker.rgbColors = palette.color;
        colorPicker.colorElementHeight = 11;
        colorPicker.colorElementWidth  = 12;
        colorPicker.inFocus = inFocus,
        colorPicker.colorIndex = effect->colorIndex;
        guiElementColorPicker(&colorPicker);
        if (!drawAll) {
            break;
        }
        
    case GUI_EVENT_DRAW_EFFECT_DURATION:
        guiElementLabeledNumericValue(&effect->durationMs, NumericValueTypeU32, "Duration:", 
                                      inFocus, NumericBaseDec, &PosEffectDuration, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_LATERAL:
        guiElementCheckBox("LA", effect->lateral, 
                           inFocus, &PosEffectLateral, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_REVERSE:
        guiElementCheckBox("RE", effect->reverse, 
                           inFocus, &PosEffectReverse, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_EFFECT_MIRROR:
        guiElementCheckBox("MI", effect->mirror, 
                           inFocus, &PosEffectMirror, &SizFiveChars);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DIAL_HINT:
        switch (dialHintState) {
        case DIAL_HINT_STATE_SAVE:
            guiElementLabel("o   Save|", false, &PosDialHint, NULL);
            break;
        case DIAL_HINT_STATE_SEND:
            guiElementLabel("o   Send|", false, &PosDialHint, NULL);
            break;
        }
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_ENTER_HINT:
        switch (enterHintState) {
        case ENTER_HINT_STATE_NONE:
            guiElementLabel("K2   Exit", false, &PosEnterHint, NULL);
            break;
        case ENTER_HINT_STATE_REVERT:
            guiElementLabel("K2 Revert", false, &PosEnterHint, NULL);
            break;
        }
        // fallthrough ends here
        break;

    case GUI_EVENT_DRAW_COMM_ERR_HINT:
        switch (arg2) {
        case 0x00:
            guiElementLabel("COMM: ERROR", true, &PosCommErrHint, NULL);
            break;
        case 0x01:
            guiElementLabel("COMM: OK", true, &PosCommErrHint, NULL);
            break;
        }
        break;

    case GUI_EVENT_DRAW_EFFECT_SETTINGS:
        appletOnGui(GUI_EVENT_DRAW_EFFECT_TYPE, 0);
        appletOnGui(GUI_EVENT_DRAW_PALETTE_INDEX, 0);
        appletOnGui(GUI_EVENT_DRAW_PALETTE, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_DURATION, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_LATERAL, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_REVERSE, 0);
        appletOnGui(GUI_EVENT_DRAW_EFFECT_MIRROR, 0);
        break;

    default:
        break;
    }
}

static void onButtonDown(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    if (settingsIndex + 1 < SettingsCount) {
        settingsIndex++;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonUp(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    if (((int32_t)settingsIndex) - 1 >= 0) {
        settingsIndex--;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonDial(void)
{
    switch (dialHintState) {
    case DIAL_HINT_STATE_SAVE:
        dialHintState = DIAL_HINT_STATE_SEND;
        enterHintState = ENTER_HINT_STATE_NONE;
        settingsSetDisplay(displayIndex, &displaySettingsSnapshot);
        settingsSetEffect(displaySettingsSnapshot.effect, &effectSettingsSnapshot);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        break;
    case DIAL_HINT_STATE_SEND:
        // send settings
        if (sendSettings()) {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_COMM_ERR_HINT, 0x01);
        } else {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_COMM_ERR_HINT, 0x00);
        }
        break;
    }
}

static void onButtonEnter(void)
{
    switch (enterHintState) {
    case ENTER_HINT_STATE_REVERT:
        // revert settings
        enterHintState = ENTER_HINT_STATE_NONE;
        dialHintState = DIAL_HINT_STATE_SEND;
        settingsGetEffect(displaySettingsSnapshot.effect, &effectSettingsSnapshot);
        settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        queueSettingsControlDraw(settingsIndex, true);
        break;
    case ENTER_HINT_STATE_NONE:
        appletManagerQueueAppletExit();
        break;
    }
}

static void onEncoder(int32_t delta)
{
    bool recheckConfig = false;
    DisplaySettings *display = &displaySettingsSnapshot;
    EffectSettings *effect = &effectSettingsSnapshot;
    Palette palette;

    switch (settingsIndex) {
    case SettingsDisplayIndex:
        if ((int32_t)displayIndex + delta < RENDERER_COUNT
            && (int32_t)displayIndex + delta >= 0) {
            displayIndex += delta;
            enterHintState = ENTER_HINT_STATE_NONE;
            dialHintState = DIAL_HINT_STATE_SEND;
            settingsGetDisplay(displayIndex, display);
            settingsGetEffect(display->effect, effect);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        }
        break;
    case SettingsEffectIndex:
        if ((int32_t)display->effect + delta < EFFECT_COUNT
            && ((int32_t)display->effect) + delta >= 0) {
            display->effect += delta;
            settingsGetEffect(display->effect, effect);
            recheckConfig = true;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_EFFECT_SETTINGS, 0);
        } 
        break;
    case SettingsEffectType:
        if ((int32_t)effect->type + delta < EffectTypeCount
            && (int32_t)effect->type + delta >= 0) {
            effect->type += delta;
            recheckConfig = true;
        }
        break;
    case SettingsEffectPaletteIndex:
        if ((int32_t)effect->palette + delta < PALETTE_COUNT
            && (int32_t)effect->palette + delta >= 0) {
            effect->palette += delta;
            effect->colorIndex = 0;
            recheckConfig = true;
            queueSettingsControlDraw(SettingsColorIndex, false);
        }
        break;
    case SettingsColorIndex:
        settingsGetPalette(effect->palette, &palette);
        if ((int32_t)effect->colorIndex + delta < palette.colorCount
            && (int32_t)effect->colorIndex + delta >= 0) {
            effect->colorIndex += delta;
            recheckConfig = true;
        }
        break;
    case SettingsEffectDuration:
        if ((int32_t)effect->durationMs + delta >= 0) {
            effect->durationMs += delta * 100;
            recheckConfig = true;
        }
        break;
    case SettingsEffectLateral:
        if (delta > 0) {
            effect->lateral = true;
        } else if (delta < 0) {
            effect->lateral = false;
        }
        recheckConfig = true;
        break;
    case SettingsEffectReverse:
        if (delta > 0) {
            effect->reverse = true;
        } else if (delta < 0) {
            effect->reverse = false;
        }
        recheckConfig = true;
        break;
    case SettingsEffectMirror:
        if (delta > 0) {
            effect->mirror = true;
        } else if (delta < 0) {
            effect->mirror = false;
        }
        recheckConfig = true;
        break;
    default:
        break;
    }

    if (recheckConfig) {
        queueSettingsControlDraw(settingsIndex, true);
        if (isConfigChanged()) {
            dialHintState = DIAL_HINT_STATE_SAVE;
            enterHintState = ENTER_HINT_STATE_REVERT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        } else {
            if (dialHintState != DIAL_HINT_STATE_SEND) {
                dialHintState = DIAL_HINT_STATE_SEND;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
            if (enterHintState != ENTER_HINT_STATE_NONE) {
                enterHintState = ENTER_HINT_STATE_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            }
        }
    }
}

static void appletOnInput(InputEvent evt)
{
    switch (evt.type) {
    case EventButtonDown:
        if (evt.buttonState) {
            onButtonDown();
        } 
        break;
    case EventButtonUp:
        if (evt.buttonState) {
            onButtonUp();
        }
        break;
    case EventButtonDial:
        if (evt.buttonState) {
            onButtonDial();
        }
        break;
    case EventButtonEnter:
        if (evt.buttonState) {
            onButtonEnter();
        }
        break;
    case EventEncoder:
        onEncoder(evt.encoderDelta);
        break;
    }
}

static void appletOnExit(void)
{
    
}


void detachedRendererStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}