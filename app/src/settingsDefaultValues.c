#include "settings.h"

#define RGB(x,y,z) {.r = (x), .g = (y), .b = (z)}

const SystemSettings defSettingsValues = {
    .renderer = {
        { .effect = 0, .viewport = 0, .mode = 0 },
        { .effect = 1, .viewport = 1, .mode = 0 },
        { .effect = 2, .viewport = 2, .mode = 0 },
        { .effect = 3, .viewport = 3, .mode = 0 },
        { .effect = 4, .viewport = 4, .mode = 0 },
    },

    .viewport = {
        { .size = 3, .offset = 0 },
        { .size = 3, .offset = 3 },
        { .size = 3, .offset = 6 },
        { .size = 3, .offset = 9 },
        { .size = 3, .offset = 12 },
    },

    .display = {
        { .channelCount = 3, .viewportOffset = 0, .effect = 0, .detached = true, .remoteGate = false, .driveType = 2,
            .deviceAddress = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF} },
        { .channelCount = 3, .viewportOffset = 3, .effect = 1, .detached = true, .remoteGate = false, .driveType = 2,
            .deviceAddress = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF} },
        { .channelCount = 3, .viewportOffset = 6, .effect = 2, .detached = true, .remoteGate = false, .driveType = 2,
            .deviceAddress = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF} },
        { .channelCount = 3, .viewportOffset = 9, .effect = 3, .detached = true, .remoteGate = false, .driveType = 2,
            .deviceAddress = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF} },
        { .channelCount = 3, .viewportOffset = 12, .effect = 4, .detached = true, .remoteGate = false, .driveType = 2,
            .deviceAddress = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF} },
    },

    .effect = {
        { .colorIndex = 0, .palette = 1, .durationMs = 1000, .type = 3, .lateral = true, .reverse = false, .mirror = false },
        { .colorIndex = 0, .palette = 0, .durationMs = 0, .type = 0, .lateral = false, .reverse = false, .mirror = false },
        { .colorIndex = 0, .palette = 0, .durationMs = 0, .type = 0, .lateral = false, .reverse = false, .mirror = false },
        { .colorIndex = 0, .palette = 0, .durationMs = 0, .type = 0, .lateral = false, .reverse = false, .mirror = false },
        { .colorIndex = 0, .palette = 0, .durationMs = 0, .type = 0, .lateral = false, .reverse = false, .mirror = false },
    },


    .palette =
    {
        { .type = PaletteTypeRGB,
          .colorCount = 7,
          .color = {
                RGB(255, 0,   0  ),
                RGB(0,   255, 0  ),
                RGB(0,   0,   255),
                RGB(255, 255, 0  ),
                RGB(255, 0,   255),
                RGB(0,   255, 255),
                RGB(255, 255, 255),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 4,
          .color = {
                RGB(255, 128, 0),
                RGB(0,   0,   0),
                RGB(255, 128, 0),
                RGB(0,   0,   0),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 4,
          .color = {
                RGB(255, 0,   255),
                RGB(0,   255, 255),
                RGB(255, 0,   255),
                RGB(0,   255, 255),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 4,
          .color = {
                RGB(255, 128, 0  ),
                RGB(255, 255, 255),
                RGB(255, 128, 0  ),
                RGB(255, 255, 255),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 5,
          .color = {
                RGB(0,   255, 128),
                RGB(255, 128, 0  ),
                RGB(0,   0,   0  ),
                RGB(0,   255, 128),
                RGB(255, 128, 0  ),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 6,
          .color = {
                RGB(255, 255, 0  ),
                RGB(255, 255, 0  ),
                RGB(255, 255, 0  ),
                RGB(255, 255, 0  ),
                RGB(255, 255, 0  ),
                RGB(255, 255, 0  ),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 6,
          .color = {
                RGB(255, 0, 0  ),
                RGB(255, 0, 0  ),
                RGB(255, 0, 0  ),
                RGB(255, 0, 0  ),
                RGB(255, 0, 0  ),
                RGB(255, 0, 0  ),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 6,
          .color = {
                RGB(0, 255, 0),
                RGB(0, 255, 0),
                RGB(0, 255, 0),
                RGB(0, 255, 0),
                RGB(0, 255, 0),
                RGB(0, 255, 0),
            }
        },
        { .type = PaletteTypeRGB,
          .colorCount = 6,
          .color = {
                RGB(0, 0, 255),
                RGB(0, 0, 255),
                RGB(0, 0, 255),
                RGB(0, 0, 255),
                RGB(0, 0, 255),
                RGB(0, 0, 255),
            }
        },
    }
};

#undef RGB
