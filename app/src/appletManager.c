#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>
#include "appletManager.h"
#include "appletTypes.h"
#include "board.h"
#include "guiElements.h"
#include "config.h"
#include "hostManager.h"
#include "hostConnection.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#define MAX_QUEUED_MESSAGES (10U)
#define HOST_CONFIG_DATA_BUFFER_SIZE (HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE)

typedef struct {
    uint32_t arg1;
    uint32_t arg2;
} QueuedMessage;

typedef struct {
    AppletCallbacks cb;
} AppletStartMessage;

typedef struct AppletLink {
    AppletCallbacks cb;
    struct AppletLink* prev;
} AppletLink;

enum SyncBit {
    SYNC_BIT_GUI_QUEUE_CLEAR_DONE = 0x01,
    SYNC_BIT_GUI_APPLET_HEAD_UPDATE_DONE = 0x02,
};

static TaskHandle_t guiTaskHandle = NULL;
static TaskHandle_t processTaskHandle = NULL;
static TaskHandle_t buttonTaskHandle = NULL;
static TaskHandle_t hostTaskHandle = NULL;
static QueueSetHandle_t processQueueSet = NULL;
static QueueSetHandle_t guiQueueSet = NULL;
static QueueHandle_t processQueue = NULL;
static QueueHandle_t guiQueue = NULL;
static QueueHandle_t appletStartQueue = NULL;
static QueueHandle_t inputQueue = NULL;
static SemaphoreHandle_t popAppletSemaphore = NULL;
static SemaphoreHandle_t guiQueueClearSemaphore = NULL;
static SemaphoreHandle_t encoderSemaphore = NULL;
static SemaphoreHandle_t guiUpdateHeadSemaphore = NULL;
static SemaphoreHandle_t appletTimerSemaphore = NULL;
static SemaphoreHandle_t hostChannelDataSemaphore = NULL;
static SemaphoreHandle_t hostControlDataSemaphore = NULL;
static SemaphoreHandle_t hostConnectionSemaphore = NULL;
static SemaphoreHandle_t hostProcessSemaphore = NULL;
static EventGroupHandle_t syncEventGroup = NULL; 
static TimerHandle_t buttonTimer = NULL;
static TimerHandle_t appletTimer = NULL;

static const uint32_t ButtonTimerPeriodMs = 100;
static AppletLink *appletHead = NULL;
static int8_t encoderState = 0;
static void (*appletTimerCbFn)(void) = NULL; 
static volatile bool hostConnectionStatus = false;
static uint8_t hostChannelDataRxBuffer[HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE];
static uint32_t hostChannelDataRxBufferReceivedLen = 0;
static uint8_t hostConfigDataRxBuffer[HOST_CONFIG_DATA_BUFFER_SIZE];
static uint32_t hostConfigDataRxBufferReceivedLen = 0;

static void encoderHandle(BoardEncoderChange change)
{    
    if (change == BOARD_ENCODER_CHANGE_INCREMENT) {
        encoderState++;
    }
    if (change == BOARD_ENCODER_CHANGE_DECREMENT) {
        encoderState--;
    }
    
    BaseType_t highprioawoken = pdFALSE;
    xSemaphoreGiveFromISR(encoderSemaphore, &highprioawoken);
    portYIELD_FROM_ISR(highprioawoken);
}

static void guiTask(void * pvParameters) {
    (void)pvParameters;

    bool skipMessageProcessing = false;
    AppletLink *currentApplet = NULL;

    guiElementInitScreen();
    for (;;) {
        QueueSetMemberHandle_t handle = xQueueSelectFromSet(guiQueueSet, portMAX_DELAY);
        if (handle == guiQueueClearSemaphore) {
            // if task needs to clear queue, set flag and send empty message to force queue processing
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            skipMessageProcessing = true;
            QueuedMessage msg;
            xQueueSend(guiQueue, &msg, pdMS_TO_TICKS(1));
        } else if (handle == guiUpdateHeadSemaphore) {
            // if task needs to swap head, do it and signal event bit about it
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            currentApplet = appletHead;
            xEventGroupSetBits(syncEventGroup, SYNC_BIT_GUI_APPLET_HEAD_UPDATE_DONE);
        } else if (handle == guiQueue) {
            QueuedMessage msg;
            if (xQueueReceive(handle, &msg, pdMS_TO_TICKS(1)) != pdPASS) {
                continue;
            }

            if (skipMessageProcessing) {
                // skip received message and if queue is empty after that - signal about empty queue
                if (uxQueueMessagesWaiting(handle) == 0) {
                    skipMessageProcessing = false;
                    xEventGroupSetBits(syncEventGroup, SYNC_BIT_GUI_QUEUE_CLEAR_DONE);
                }
                continue;
            }

            // process current head if messages arrived
            if (currentApplet != NULL) {
                if (currentApplet->cb.onGui != NULL) {
                    currentApplet->cb.onGui(msg.arg1, msg.arg2);
                }
            }
        }
    }
}

static void pendGuiQueueClearAndWait(void)
{
    xSemaphoreGive(guiQueueClearSemaphore);
    xEventGroupWaitBits(syncEventGroup, SYNC_BIT_GUI_QUEUE_CLEAR_DONE, pdTRUE, pdTRUE, portMAX_DELAY);
}

static void pendGuiAppletHeadSwapAndWait(void)
{
    xSemaphoreGive(guiUpdateHeadSemaphore);
    xEventGroupWaitBits(syncEventGroup, SYNC_BIT_GUI_APPLET_HEAD_UPDATE_DONE, pdTRUE, pdTRUE, portMAX_DELAY);
}

static void hostChannelDataRxCb(uint8_t *data, size_t len)
{
    if (len > HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE) {
        assert(0);
    }
    memcpy(hostChannelDataRxBuffer, data, len);
    hostChannelDataRxBufferReceivedLen = len;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(hostChannelDataSemaphore, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void hostControlDataRxCb(uint8_t *data, size_t len)
{
    (void)data;
    (void)len;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(hostControlDataSemaphore, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void hostConnectionCb(bool connected)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(hostConnectionSemaphore, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    hostConnectionStatus = connected;
}

static void processTask(void * pvParameters) {
    (void)pvParameters;

    static uint8_t rxBuffer[HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE];
    uint32_t rxLen;
    bool skipMessagesAndPopApplet = false;
    boardSetEncoderChangeCb(encoderHandle);
    // init usb/cdc
    // init usb/vendor hid
    // listen for packets and events
    hostManagerInit(hostChannelDataRxCb, 
                    hostControlDataRxCb,
                    hostConnectionCb,
                    HOST_MANAGER_CHANNEL_PROTOCOL_RENARD);

    for (;;) {
        QueueSetMemberHandle_t handle = xQueueSelectFromSet(processQueueSet, portMAX_DELAY);
        if (handle == processQueue) {
            QueuedMessage msg;
            if (xQueueReceive(handle, &msg, pdMS_TO_TICKS(1)) != pdPASS) {
                continue;
            }

            if (skipMessagesAndPopApplet) {
                // if skip-and-pop is set, wait if queue is empty
                if (uxQueueMessagesWaiting(handle) == 0) {
                    // when queue is empty, call gui task to clear its message queue
                    // call current applet onExit callback
                    // swap global head with previous applet from current applet
                    // make gui task swap heads
                    // assign current applet
                    // call onEnter for new current applet
                    // free memory of old applet
                    pendGuiQueueClearAndWait();
                    appletTimerCbFn = NULL;
                    if (appletHead->cb.onExit) {
                        appletHead->cb.onExit();
                    }
                    AppletLink *oldApplet = appletHead;
                    appletHead = appletHead->prev; 
                    pendGuiAppletHeadSwapAndWait();
                    if (appletHead != NULL && appletHead->cb.onEnter) {
                        appletHead->cb.onEnter();
                    }
                    appletManagerFreeMemory(oldApplet);
                    skipMessagesAndPopApplet = false;
                }
                continue;
            }

            // process current head if messages arrived
            if (appletHead != NULL && appletHead->cb.onProcess != NULL) {
                appletHead->cb.onProcess(msg.arg1, msg.arg2);
            }
        } else if (handle == inputQueue) {
            InputEvent evt;
            if (xQueueReceive(handle, &evt, pdMS_TO_TICKS(1)) != pdPASS) {
                continue;
            }

            // process current head if messages arrived
            if (appletHead != NULL && appletHead->cb.onInput != NULL) {
                appletHead->cb.onInput(evt);
            }
        } else if (handle == appletStartQueue) {
            AppletStartMessage msg;
            if (xQueueReceive(handle, &msg, pdMS_TO_TICKS(1)) != pdPASS) {
                continue;
            }

            // create new applet link
            // fill it with data about new applet callbacks
            // push it to applet list
            // make gui task clear message queue
            // make gui task swap applet heads
            // call applet onEnter
            AppletLink *newAppletHead = (AppletLink *)appletManagerRequestMemory(sizeof(AppletLink));
            if (newAppletHead == NULL) {
                continue;
            }

            newAppletHead->cb = msg.cb;
            newAppletHead->prev = appletHead;
            xSemaphoreGive(guiQueueClearSemaphore);
            xEventGroupWaitBits(syncEventGroup, SYNC_BIT_GUI_QUEUE_CLEAR_DONE, pdTRUE, pdTRUE, portMAX_DELAY);
            appletTimerCbFn = NULL;
            if (appletHead != NULL && appletHead->cb.onExit != NULL) {
                appletHead->cb.onExit();
            }
            appletHead = newAppletHead;
            xSemaphoreGive(guiUpdateHeadSemaphore);
            xEventGroupWaitBits(syncEventGroup, SYNC_BIT_GUI_APPLET_HEAD_UPDATE_DONE, pdTRUE, pdTRUE, portMAX_DELAY);
            if (appletHead != NULL && appletHead->cb.onEnter != NULL) {
                appletHead->cb.onEnter();
            }
        } else if (handle == popAppletSemaphore) {
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            skipMessagesAndPopApplet = true;
            QueuedMessage msg;
            xQueueSend(processQueue, &msg, pdMS_TO_TICKS(1));
        } else if (handle == encoderSemaphore) {
            InputEvent evt;
            bool giveSem = false;

            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            evt.encoderDelta = encoderState;
            evt.type = EventEncoder;
            if (appletHead != NULL && appletHead->cb.onInput != NULL) {
                appletHead->cb.onInput(evt);
            }
            taskENTER_CRITICAL();
            
            if (encoderState > 0) {
                encoderState--;
            } else if (encoderState < 0) {
                encoderState++;
            }
            if (encoderState != 0) {
                giveSem = true;
            }
            taskEXIT_CRITICAL();
            if (giveSem) {
                xSemaphoreGive(handle);
            }
        } else if (handle == appletTimerSemaphore) {
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            if (appletTimerCbFn) {
                appletTimerCbFn();
                appletTimerCbFn = NULL;
            }
        } else if (handle == hostProcessSemaphore) {
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
        } else if (handle == hostConnectionSemaphore) {
            // connection changed - port opened or closed
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            taskENTER_CRITICAL();
            bool connected = hostConnectionStatus;
            taskEXIT_CRITICAL();
            if (connected) {
                // exit current applet
                // or ignore connection if applet is unexitable
                // enter host-connection applet
                // and wait until it starts
                hostConnectionStart();
            } else {
                // exit host-connection applet
                hostConnectionStop();
            }
        } else if (handle == hostChannelDataSemaphore
                    || handle == hostControlDataSemaphore) {
            xSemaphoreTake(handle, pdMS_TO_TICKS(1));
            uint32_t *len = NULL;
            uint8_t *buf = NULL;
            AppletHostDataType type;
            if (handle == hostChannelDataSemaphore) {
                len = &hostChannelDataRxBufferReceivedLen;
                buf = hostChannelDataRxBuffer;
                type = APPLET_HOST_DATA_TYPE_CHANNEL;
            } else if (handle == hostControlDataSemaphore) {
                len = &hostConfigDataRxBufferReceivedLen;
                buf = hostConfigDataRxBuffer;
                type = APPLET_HOST_DATA_TYPE_CONFIG;
            }
            assert(len != NULL && buf != NULL);

            taskENTER_CRITICAL();
            rxLen = *len;
            if (rxLen > HOST_MANAGER_CHANNEL_DATA_BUFFER_SIZE) {
                assert(0);
            }
            memcpy(rxBuffer, buf, rxLen);
            *len = 0;
            taskEXIT_CRITICAL();
            if (rxLen == 0) {
                continue;
            }
            // send data to applets
            if (appletHead != NULL 
                && appletHead->cb.onHostDataEvent != NULL) {
                appletHead->cb.onHostDataEvent(type, rxBuffer, rxLen);
            }
        }
    }
}

static void buttonTimerCb(TimerHandle_t t)
{
    xTaskNotifyGive(buttonTaskHandle);
}

static void buttonTask(void *pvParam)
{
    bool pinState[BOARD_PIN_COUNT] = {0};

    xTimerStart(buttonTimer, pdMS_TO_TICKS(1));
    for (uint32_t i = 0; i < BOARD_PIN_COUNT; i++) {
        pinState[i] = boardGetPin(i);
    }
    for (;;) {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        for (uint32_t i = 0; i < BOARD_PIN_COUNT; i++) {
            bool newState = boardGetPin(i);
            if (newState != pinState[i]) {
                InputEvent evt;
                switch (i) {
                case BOARD_PIN_BUTTON_DIAL:
                    evt.type = EventButtonDial;
                    break;
                case BOARD_PIN_BUTTON_DOWN:
                    evt.type = EventButtonDown;
                    break;
                case BOARD_PIN_BUTTON_UP:
                    evt.type = EventButtonUp;
                    break;
                case BOARD_PIN_BUTTON_ENTER:
                    evt.type = EventButtonEnter;
                    break;
                }
                evt.buttonState = !newState;
                xQueueSend(inputQueue, &evt, pdMS_TO_TICKS(1));
                pinState[i] = newState;
            }
        }
    }
}

static void appletTimerCb(TimerHandle_t t)
{
    xSemaphoreGive(appletTimerSemaphore);
}

void appletManagerStart(void)
{
    // queue + queue + queue + 5 semaphores
    const uint32_t QueueSetMsgCount = MAX_QUEUED_MESSAGES + MAX_QUEUED_MESSAGES + 1;
    processQueueSet = xQueueCreateSet(QueueSetMsgCount + 7);
    processQueue = xQueueCreate(MAX_QUEUED_MESSAGES, sizeof(QueuedMessage));
    assert(xQueueAddToSet(processQueue, processQueueSet) == pdPASS);
    popAppletSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(popAppletSemaphore, processQueueSet) == pdPASS);
    appletStartQueue = xQueueCreate(1, sizeof(AppletStartMessage));
    assert(xQueueAddToSet(appletStartQueue, processQueueSet) == pdPASS);
    inputQueue = xQueueCreate(MAX_QUEUED_MESSAGES, sizeof(InputEvent));
    assert(xQueueAddToSet(inputQueue, processQueueSet) == pdPASS);
    encoderSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(encoderSemaphore, processQueueSet) == pdPASS);
    appletTimer = xTimerCreate("appletTimer", pdMS_TO_TICKS(1), pdFALSE, NULL, appletTimerCb);
    appletTimerSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(appletTimerSemaphore, processQueueSet) == pdPASS);
    hostChannelDataSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(hostChannelDataSemaphore, processQueueSet) == pdPASS);
    hostControlDataSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(hostControlDataSemaphore, processQueueSet) == pdPASS);
    hostConnectionSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(hostConnectionSemaphore, processQueueSet) == pdPASS);
    hostProcessSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(hostProcessSemaphore, processQueueSet) == pdPASS);

    // queue + semaphore + semaphore
    guiQueueSet = xQueueCreateSet(MAX_QUEUED_MESSAGES + 2);
    guiQueue = xQueueCreate(MAX_QUEUED_MESSAGES, sizeof(QueuedMessage));
    assert(xQueueAddToSet(guiQueue, guiQueueSet) == pdPASS);
    guiQueueClearSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(guiQueueClearSemaphore, guiQueueSet) == pdPASS);
    guiUpdateHeadSemaphore = xSemaphoreCreateBinary();
    assert(xQueueAddToSet(guiUpdateHeadSemaphore, guiQueueSet) == pdPASS);

    syncEventGroup = xEventGroupCreate();
    buttonTimer = xTimerCreate(NULL, pdMS_TO_TICKS(ButtonTimerPeriodMs), pdTRUE, 
                               NULL, buttonTimerCb);

    xTaskCreate(guiTask,
                "lcd",
                configMINIMAL_STACK_SIZE * 2,
                NULL,
                TaskPriorityGui,
                &guiTaskHandle
                );

    xTaskCreate(processTask,
                "process",
                configMINIMAL_STACK_SIZE * 4,
                NULL,
                TaskPriorityProcess,
                &processTaskHandle
                );

    xTaskCreate(buttonTask,
                "button",
                configMINIMAL_STACK_SIZE,
                NULL,
                TaskPriorityButton,
                &buttonTaskHandle
                );
}

void appletManagerQueueGuiEvent(uint32_t arg1, uint32_t arg2)
{
    assert(guiQueue != NULL);

    QueuedMessage msg = {arg1, arg2};
    xQueueSendToBack(guiQueue, &msg, portMAX_DELAY);
}

void appletManagerQueueProcessEvent(uint32_t arg1, uint32_t arg2)
{
    assert(processQueue != NULL);

    QueuedMessage msg = {arg1, arg2};
    xQueueSendToBack(processQueue, &msg, pdMS_TO_TICKS(1));
}

void appletManagerQueueAppletExit(void)
{
    assert(popAppletSemaphore != NULL);

    xSemaphoreGive(popAppletSemaphore);
}

void appletManagerSleep(uint32_t timeMs)
{
    vTaskDelay(pdMS_TO_TICKS(timeMs));
}

void appletManagerStartApplet(AppletCallbacks *cb)
{
    assert(appletStartQueue != NULL);

    AppletStartMessage msg;
    msg.cb = *cb;
    xQueueSend(appletStartQueue, &msg, portMAX_DELAY);
}

void *appletManagerRequestMemory(size_t size)
{
    return pvPortMalloc(size);
}

void appletManagerFreeMemory(void *ptr)
{
    vPortFree(ptr);
}

void appletManagerStartTimer(uint32_t timeoutMs, void (*callback)(void))
{
    if (xTimerIsTimerActive(appletTimer) == pdTRUE) {
        xTimerStop(appletTimer, pdMS_TO_TICKS(1));
    }
    appletTimerCbFn = callback;
    xTimerChangePeriod(appletTimer, pdMS_TO_TICKS(timeoutMs), pdMS_TO_TICKS(1));
}