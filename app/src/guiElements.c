#include "guiElements.h"
#include "nrf_font.h"
#include "nrf_lcd.h"
#include "nrf_gfx.h"
#include "board.h"
#include "assert.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define COLOR_BLACK (0x0000U)
#define COLOR_WHITE (0xFFFFU)
#define COLOR_CYAN (0xFFFFU)

static uint16_t rgb24to16(RgbColor *rgb);

extern nrf_lcd_t nrf_lcd_ili9341;
extern FONT_INFO orkney_8ptFontInfo;
extern FONT_INFO orkney_24ptFontInfo;
extern FONT_INFO lucidaConsole_8ptFontInfo;

static const FONT_INFO *TextFont = &lucidaConsole_8ptFontInfo;

static void fillRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color)
{
    nrf_gfx_rect_t rect = {
        .x = x0,
        .y = y0,
        .width = w,
        .height = h
    };
    assert(nrf_gfx_rect_draw(&nrf_lcd_ili9341, &rect, 1, color, true) == NRF_SUCCESS);
}

static void putString8(const char *str, uint16_t x0, uint16_t y0, uint16_t color)
{
    nrf_gfx_point_t pt = {
        .x = x0,
        .y = y0
    };
    assert(nrf_gfx_print(&nrf_lcd_ili9341, &pt, color, str, TextFont, true) == NRF_SUCCESS);
}

static void getStringSize(const char *str, FONT_INFO *font, uint16_t *width, uint16_t *height)
{
    uint32_t wval = 0;
    uint32_t hval = font->height;
    while (*str != '\0') {
        char c = *str;
        switch (c) {
        case '\n':
            hval += font->height;
            break;
        case ' ':
            // as in string print function
            wval += font->height / 2;
            break;
        default:
            if (c >= font->startChar && c <= font->endChar) {
                wval += font->charInfo[c - font->startChar].widthBits;
                wval += font->spacePixels;
            }
            break;
        }
        str++;
    }
    *width = wval;
    *height = hval;
}

static void lineDraw(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
{
    nrf_gfx_line_t line = {
        .thickness = 1,
        .x_start = x0,
        .x_end = x1,
        .y_start = y0,
        .y_end = y1
    };
    assert(nrf_gfx_line_draw(&nrf_lcd_ili9341, &line, color) == NRF_SUCCESS);
}

void guiElementInitScreen(void)
{
    assert(nrf_gfx_init(&nrf_lcd_ili9341) == NRF_SUCCESS);
    // MY: 1, MV: 1, BGR: 1 for this 128x128 board
    nrf_gfx_rotation_set(&nrf_lcd_ili9341, NRF_LCD_ROTATE_0);
}

void guiElementFillBackground(void)
{
    nrf_gfx_screen_fill(&nrf_lcd_ili9341, COLOR_WHITE);
}

void guiElementColorPicker(ColorPickerSettings *settings)
{
    const uint32_t selectorWidth = settings->colorElementWidth / 4;
    const uint32_t selectorHeight = settings->colorElementHeight / 2;

    if (settings == NULL || settings->position == NULL 
        || (settings->isHsv && settings->hsvColors == NULL)
        || (!settings->isHsv && settings->rgbColors == NULL))
        return;

    if (settings->size) {
        fillRect(settings->position->x, settings->position->y, 
                 settings->size->x, settings->size->y, COLOR_WHITE);
    }

    for (uint32_t i = 0; i < settings->colorCount; i++) {
        RgbColor rgb;
        if (settings->isHsv)
            HsvToRgb(&settings->hsvColors[i], &rgb);
        else
            rgb = settings->rgbColors[i];
        bool isCurrentlySelected = settings->inFocus && i == settings->colorIndex;
        fillRect(settings->position->x + i * settings->colorElementWidth,
                 settings->position->y,
                 settings->colorElementWidth,
                 settings->colorElementHeight,
                 rgb24to16(&rgb));
        if (isCurrentlySelected) {
            fillRect(settings->position->x + (i + 1) * settings->colorElementWidth 
                     - settings->colorElementWidth / 2 - selectorWidth,
                     settings->position->y + settings->colorElementHeight - selectorHeight,
                     selectorWidth,
                     selectorHeight,
                     COLOR_BLACK);
            fillRect(settings->position->x + (i + 1) * settings->colorElementWidth 
                     - settings->colorElementWidth / 2,
                     settings->position->y + settings->colorElementHeight - selectorHeight,
                     selectorWidth,
                     selectorHeight,
                     COLOR_WHITE);
        }
    }

    lineDraw(settings->position->x, settings->position->y, 
             settings->position->x + settings->colorCount * settings->colorElementWidth,
             settings->position->y, COLOR_BLACK);
    lineDraw(settings->position->x, settings->position->y + settings->colorElementHeight, 
             settings->position->x + settings->colorCount * settings->colorElementWidth,
             settings->position->y + settings->colorElementHeight, COLOR_BLACK);
    lineDraw(settings->position->x, settings->position->y, 
             settings->position->x,
             settings->position->y + settings->colorElementHeight, COLOR_BLACK);
    lineDraw(settings->position->x + settings->colorCount * settings->colorElementWidth, settings->position->y, 
             settings->position->x + settings->colorCount * settings->colorElementWidth,
             settings->position->y + settings->colorElementHeight, COLOR_BLACK);
}

void guiElementCheckBox(const char *label, bool isChecked, bool inFocus, 
                        const Point2d *position, const Point2d *size)
{
    if (position == NULL || label == NULL)
        return;

    static const char StrChecked[] = "[x]";
    static const char StrUnchecked[] = "[_]";

    guiElementLabeledString(label, isChecked ? StrChecked : StrUnchecked, inFocus, position, size);    
}

void guiElementLabeledNumericValue(void *value, NumericValueType type, const char *label, bool inFocus, NumericBase base,
                                   const Point2d *position, const Point2d *size)
{
    #define MAX_LABEL_STRING_LENGTH   20
    if (value == NULL || position == NULL || label == NULL)
        return;

    uint32_t val;
    char valString[MAX_LABEL_STRING_LENGTH];

    memset(valString, 0, sizeof(valString));
    switch (type) {
        case NumericValueTypeU32:
            itoa(*((uint32_t *)value), valString, base);
            break;
        case NumericValueTypeU8:
            itoa(*((uint8_t *)value), valString, base);
            break;
        default:
            break;
    }

    guiElementLabeledString(label, valString, inFocus, position, size);
}

void guiElementLabel(const char * label, bool inFocus, 
                     const Point2d *position, const Point2d *size)
{
    if (label == NULL || position == NULL)
        return;

    Point2d labelSize;
    if (size == NULL) {
        guiElementLabelGetSize(label, &labelSize);
    } else {
        labelSize = *size;
    }
    fillRect(position->x, position->y, labelSize.x, labelSize.y, 
             inFocus ? COLOR_BLACK : COLOR_WHITE);
    putString8(label,
                position->x,
                position->y,
                inFocus ? COLOR_WHITE : COLOR_BLACK);
}

void guiElementList(const char * strings[], uint32_t count, int32_t index, const char * marker, 
                    const Point2d *position, const Point2d *size)
{
    if (strings == NULL || position == NULL)
        return;
        
    static const char EmptyMarker[] = "";
    if (marker == NULL) {
        marker = EmptyMarker;
    }

    Point2d labelPosition = *position;
    Point2d labelSize;
    // TODO: add wrap of each element if does not fit screen
    if (size == NULL) {
        guiElementLabelGetSize(strings[0], &labelSize);
    } else {
        labelSize = *size;
    }
    for (uint32_t i = 0; i < count; i++) {
        bool inFocus = index >= 0 && (uint32_t)index == i;
        guiElementLabeledString(marker, strings[i], inFocus, &labelPosition, size);
        labelPosition.y += labelSize.y;
    }
}

void guiElementLabelGetSize(const char * label, Point2d *size)
{
    uint16_t height, width;
    getStringSize(label, TextFont, &width, &height);
    size->x = width;
    size->y = height;
}

static uint16_t rgb24to16(RgbColor * rgb)
{
    uint16_t result = (uint16_t)((rgb->r >> 3)<<12);
    result |= (uint16_t)((rgb->g >> 2) << 5);
    result |= (uint16_t)(rgb->b >> 3);
    return result;
}

void guiElementLabeledString(const char *label, const char *string, bool inFocus, 
                             const Point2d *position, const Point2d *size)
{
    if (label == NULL || string == NULL || position == NULL) {
        return;
    }

    Point2d labelSize;
    Point2d stringSize;
    Point2d stringPosition = *position;
    
    guiElementLabelGetSize(label, &labelSize);
    if (size == NULL) {
        guiElementLabelGetSize(string, &stringSize);
    } else {
        labelSize.y = size->y;
        stringSize.y = size->y;
        stringSize.x = size->x - labelSize.x;
    }
    stringPosition.x += labelSize.x;
    // TODO: add wrap if collide with screen width
    guiElementLabel(label, inFocus, position, &labelSize);
    guiElementLabel(string, inFocus, &stringPosition, &stringSize);
}