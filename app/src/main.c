#include "app.h"

/* The prototype shows it is a naked function - in effect this is just an
assembly function. */
void HardFault_Handler( void ) __attribute__( ( naked ) );

void prvGetRegistersFromStack( uint32_t *pulFaultStackAddress );

int main(void) {
    appStart();
}



void HardFault_Handler(void)
{
    __asm volatile(
        "movs r0, #4\t\n"
        "mov  r1, lr\t\n"
        "tst  r0, r1\t\n" /* Check EXC_RETURN[2] */
        "beq 1f\t\n"
        "mrs r0, psp\t\n"
        "ldr r1,=hard_fault_handler_c\t\n"
        "bx r1\t\n"
        "1:mrs r0,msp\t\n"
        "ldr r1,=hard_fault_handler_c\t\n"
        "bx r1\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}

void hard_fault_handler_c(const unsigned int * hardfault_args)
{
    static volatile unsigned int stacked_r0;
    static volatile unsigned int stacked_r1;
    static volatile unsigned int stacked_r2;
    static volatile unsigned int stacked_r3;
    static volatile unsigned int stacked_r12;
    static volatile unsigned int stacked_lr;
    static volatile unsigned int stacked_pc;
    static volatile unsigned int stacked_psr;

    stacked_r0 = ((unsigned long) hardfault_args[0]);
    stacked_r1 = ((unsigned long) hardfault_args[1]);
    stacked_r2 = ((unsigned long) hardfault_args[2]);
    stacked_r3 = ((unsigned long) hardfault_args[3]);

    stacked_r12 = ((unsigned long) hardfault_args[4]);
    stacked_lr = ((unsigned long) hardfault_args[5]);
    stacked_pc = ((unsigned long) hardfault_args[6]);
    stacked_psr = ((unsigned long) hardfault_args[7]);

    while(1) {
        __asm volatile ( "NOP" );
    }
    UNUSED(stacked_r0);
    UNUSED(stacked_r1);
    UNUSED(stacked_r2);
    UNUSED(stacked_r3);
    UNUSED(stacked_r12);
    UNUSED(stacked_lr);
    UNUSED(stacked_pc);
    UNUSED(stacked_psr);
}