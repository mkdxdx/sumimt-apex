#include <stdint.h>
#include "mainMenu.h"
#include "appletTypes.h"
#include "appletManager.h"
#include "guiElements.h"
#include "displaySettings.h"
#include "rendererControl.h"
#include "detachedRenderer.h"
#include "paletteSettings.h"
#include "viewportSettings.h"

enum GuiCommand {
    GUI_COMMAND_SET_BACKGROUND,
    GUI_COMMAND_DRAW_MENU
};

static int32_t menuIndex = 0;
static const char *MenuEntries[] = {
    "Linked renderer",
    "Det. renderer",
    "Set palette",
    "Set display",
    "Set viewport",
    "Configuration",
    "Status",
    "Preview"
};

static const void (*AppletStartFn[ELEMENTS(MenuEntries)])(void) = {
    rendererControlStart,
    detachedRendererStart,
    paletteSettingsStart,
    displaySettingsStart,
    viewportSettingsStart,
    NULL,
    NULL,
    NULL,
};

static void appletOnGui(uint32_t arg1, uint32_t arg2) 
{
    static const Point2d TitlePosition = { 5, 5};
    static const Point2d ListPosition = {.x = 0, .y = 15};
    static const char Marker[] = ">";
    static const char TitleLabel[] = "Select mode:";

    switch ((enum GuiCommand)arg1) {
    case GUI_COMMAND_SET_BACKGROUND:
        guiElementFillBackground();
        guiElementLabel(TitleLabel, false, &TitlePosition, NULL);
        break;
    case GUI_COMMAND_DRAW_MENU:
        guiElementList(MenuEntries, ELEMENTS(MenuEntries),
                       (int32_t)arg2, Marker, &ListPosition, NULL);
        break;
    }
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2) 
{
    (void)arg1;
    (void)arg2;
}

static void appletOnEnter(void) 
{
    menuIndex = 0;
    appletManagerQueueGuiEvent(GUI_COMMAND_SET_BACKGROUND, 0);
    appletManagerQueueGuiEvent(GUI_COMMAND_DRAW_MENU, (uint32_t)menuIndex);
}

static void appletOnExit(void) 
{

}

static void appletOnInput(InputEvent evt) 
{
    switch (evt.type) {
    case EventButtonDial:
        if (AppletStartFn[menuIndex] != NULL) {
            AppletStartFn[menuIndex]();
        }
        break;
    case EventButtonDown:
        if (evt.buttonState && menuIndex + 1 < ELEMENTS(MenuEntries)) {
            menuIndex++;
            appletManagerQueueGuiEvent(GUI_COMMAND_DRAW_MENU, 
                                       (uint32_t)menuIndex);
        }
        break;
    case EventButtonUp:
        if (evt.buttonState && menuIndex - 1 >= 0) {
            menuIndex--;
            appletManagerQueueGuiEvent(GUI_COMMAND_DRAW_MENU, 
                                       (uint32_t)menuIndex);
        }
        break;
    default:
        break;
    }
}

void mainMenuStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}