#pragma once

#include "paletteTypes.h"
#include <stdbool.h>

typedef enum EffectType {
    EffectTypeNone,             /*! No effect */
    EffectTypeFill,             /*! Fills all channels with a set color */
    EffectTypeSwitch,           /*! Switches colors with a period,
                                    with blanking period if periodOffMs is greater than 0
                                */
    EffectTypeFade,             /*! Smooth transition between colors,
                                    fades to black between colors if periodOffMs is greater than 0
                                 */
    EffectTypeChase,            /*! Color runs along channels with a tail,
                                    tail fades to end if set,
                                    can originate from one end, other end, both ends or center
                                */
    EffectTypeRainbow,          /*! Smooth transition between all rainbow colors,
                                    plane can be rotated to get running rainbow
                                */
	EffectTypeCount
} EffectType;

typedef struct EffectSettingsFill {
    unsigned int colorIndex;
} EffectSettingsFill;

typedef struct EffectSettingsSwitch {
    unsigned int durationMs;
    bool         lateral;
    bool         reverse;
    bool         mirror;
} EffectSettingsSwitch;

typedef struct EffectSettingsFade {
    unsigned int durationMs;
    bool         lateral;
    bool         reverse;
    bool         mirror;
} EffectSettingsFade;

typedef struct EffectSettingsChase {
    unsigned int durationMs;
    bool         lateral;
    bool         reverse;
    bool         mirror;
} EffectSettingsChase;

typedef struct EffectSettingsRainbow {
    unsigned int durationMs;
    bool         lateral;
} EffectSettingsRainbow;

typedef struct Effect {
    Palette * palette;
    union {
        EffectSettingsFill    settingsFill;
        EffectSettingsSwitch  settingsSwitch;
        EffectSettingsFade    settingsFade;
        EffectSettingsChase   settingsChase;
        EffectSettingsRainbow settingsRainbow;
    };
    EffectType type;
} Effect;
