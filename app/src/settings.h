#pragma once

#include "color.h"
#include <stdbool.h>
#include <stdint.h>
#include "rendererTypes.h"
#include "effectTypes.h"
#include "paletteTypes.h"

#define PALETTE_COUNT          9
#define RENDERER_COUNT         5
#define VIEWPORT_COUNT         5
#define EFFECT_COUNT           5
#define DISPLAY_COUNT          5
#define DISPLAY_ADDRESS_LENGTH 5
#define VIEWPORT_POOL_SIZE 2048

typedef struct RendererSettings {
    uint32_t effect;
    uint32_t viewport;
    uint8_t  mode;
} RendererSettings;

typedef struct EffectSettings {
    uint32_t colorIndex;
    uint32_t palette;
    uint32_t durationMs;
    EffectType   type;
    bool         lateral;
    bool         reverse;
    bool         mirror;
} EffectSettings;

typedef struct ViewportSettings {
    uint32_t size;
    uint32_t offset;
} ViewportSettings;

typedef struct DisplaySettings {
    uint32_t  channelCount;
    uint32_t  viewportOffset;
    uint32_t  effect;
    uint32_t  driveType;
    unsigned char deviceAddress[DISPLAY_ADDRESS_LENGTH];
    bool         remoteGate;
    bool      detached;
} DisplaySettings;

typedef struct SystemSettings {
    Palette          palette[PALETTE_COUNT];
    RendererSettings renderer[RENDERER_COUNT];
    ViewportSettings viewport[VIEWPORT_COUNT];
    DisplaySettings  display[DISPLAY_COUNT];
    EffectSettings   effect[EFFECT_COUNT];
} SystemSettings;

typedef void (*SettingsLockCallback)(bool lock);

void settingsInit(SettingsLockCallback lockCb);

void settingsGetPalette(uint32_t index, Palette * settings);
void settingsGetRenderer(uint32_t index, RendererSettings * settings);
void settingsGetViewport(uint32_t index, ViewportSettings * settings);
void settingsGetDisplay(uint32_t index, DisplaySettings * settings);
void settingsGetEffect(uint32_t index, EffectSettings * settings);

void settingsSetPalette(uint32_t index, Palette * settings);
void settingsSetRenderer(uint32_t index, RendererSettings * settings);
void settingsSetViewport(uint32_t index, ViewportSettings * settings);
void settingsSetDisplay(uint32_t index, DisplaySettings * settings);
void settingsSetEffect(uint32_t index, EffectSettings * settings);

void settingsSetDefault(void);


