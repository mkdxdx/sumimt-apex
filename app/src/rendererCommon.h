#pragma once

#include "settings.h"
#include "effectTypes.h"
#include <stdint.h>
#include <stdbool.h>

bool isEffectSettingValid(EffectType effect, uint32_t settingIndex);
