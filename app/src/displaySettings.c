#include "displaySettings.h"
#include "displayMode.h"
#include "common.h"
#include "settings.h"
#include "guiElements.h"
#include "displayManager.h"
#include "appletManager.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define GLOBAL_X_OFFSET (4U)

enum GuiEvent {
    GUI_EVENT_CLEAR_SCREEN,
    GUI_EVENT_DRAW_ALL,
    GUI_EVENT_DRAW_DISPLAY_INDEX,
    GUI_EVENT_DRAW_DISPLAY_OFFSET,
    GUI_EVENT_DRAW_DISPLAY_SIZE,
    GUI_EVENT_DRAW_DRIVE_TYPE,
    GUI_EVENT_DRAW_GATE_OVERRIDE,
    GUI_EVENT_DRAW_DETACHED,
    GUI_EVENT_DRAW_ADDRESS,
    GUI_EVENT_DRAW_DIAL_HINT,
    GUI_EVENT_DRAW_COMM_ERR_HINT,
    GUI_EVENT_DRAW_ENTER_HINT,

    GUI_EVENT_DRAW_FOCUS_BIT = 0x80U,
};

enum ProcessEvent {
    PROCESS_EVENT_PROBE_PAIRING,
};

enum DriveType {
    DriveTypeNone,
    DriveTypePWM,
    DriveTypePixel1,
    DriveTypePixel2,
    DriveTypeSerial1,
    DriveTypeSerial2,
    DriveTypeCount
};

static const Point2d SizHalfLine = {64, 12};
static const Point2d SizFullLine = {128, 12};
static const Point2d SizThreeChars = {21, 12};

static const Point2d PosDisplayIndex = {GLOBAL_X_OFFSET, 2};
static const Point2d PosDisplayOffset = {GLOBAL_X_OFFSET, 14};
static const Point2d PosDisplaySize = {GLOBAL_X_OFFSET + 64, 14};
static const Point2d PosDriveType = {GLOBAL_X_OFFSET, 26};
static const Point2d PosGateOverride = {GLOBAL_X_OFFSET, 38};
static const Point2d PosDetached = {GLOBAL_X_OFFSET, 50};
static const Point2d PosAddress = {GLOBAL_X_OFFSET, 62};
static const Point2d PosDialHint = {GLOBAL_X_OFFSET, 116};
static const Point2d PosEnterHint = {GLOBAL_X_OFFSET + 64, 116};
static const Point2d PosCommErrHint = {GLOBAL_X_OFFSET, 98};

static const char * DisplayDriveTypeStrings[] = {
        "None",         // no drive connected
        "PWM",          // pwm up to 3 channels
        "Pixel 1",      // WS12xx single wire
        "Pixel 2",      // WS12xx double wire
        "Serial 1",     // i2c driver
        "Serial 2",     // spi driver
        "PWM Int.",     // onboard pwm led
    };

static enum SettingsIndex {
    SettingsIndexDisplay,
    SettingsIndexDisplayVpOffset,
    SettingsIndexDisplayChannelCount,
    SettingsIndexDrive,
    SettingsIndexRemoteGate,
    SettingsIndexIsDetached,
    SettingsIndexDevAddress,
    SettingsIndexCount
} settingsIndex = SettingsIndexDisplay;
static uint32_t displayIndex  = 0;
static bool pairingRunning = false;
static DisplaySettings displaySettingsSnapshot;
static bool probeSuccess = false;
static enum DialHintState {
    DIAL_HINT_STATE_SEND,
    DIAL_HINT_STATE_UNBIND,
    DIAL_HINT_STATE_SAVE
} dialHintState = DIAL_HINT_STATE_SEND;
static enum EnterHintState {
    ENTER_HINT_STATE_NONE,
    ENTER_HINT_STATE_ABORT,
    ENTER_HINT_STATE_REVERT
} enterHintState = ENTER_HINT_STATE_NONE;

static void addressUpd(const uint8_t *data, uint32_t size)
{
    if (size == DISPLAY_ADDRESS_LENGTH) {
        memcpy(displaySettingsSnapshot.deviceAddress, data, 
               sizeof(displaySettingsSnapshot.deviceAddress));
        probeSuccess = true;
    }
}

static bool sendSettings(void)
{
    static const DisplayDriverType DriverTypeMap[] = {
        [DriveTypeNone] = DisplayDriverTypeNone,
        [DriveTypePWM] = DisplayDriverTypePWM,
        [DriveTypePixel1] = DisplayDriverTypePixel1,
        [DriveTypePixel2] = DisplayDriverTypePixel2,
        [DriveTypeSerial1] = DisplayDriverTypeSerial1,
        [DriveTypeSerial2] = DisplayDriverTypeSerial2,
    };
    DisplaySettings settings;
    DisplayConfig cfg;
    settingsGetDisplay(displayIndex, &settings);
    cfg.channelCount = settings.channelCount;
    cfg.gateOverride = settings.remoteGate;
    cfg.driverType = DriverTypeMap[settings.driveType];
    return displayManagerSendConfig(displayIndex, &cfg);
}

static bool isConfigChanged(void)
{
    DisplaySettings settings;
    settingsGetDisplay(displayIndex, &settings);
    return 0 != memcmp(&displaySettingsSnapshot, &settings, 
                       sizeof(displaySettingsSnapshot));
}

static void queueSettingsControlDraw(enum SettingsIndex index, bool focus)
{
    uint32_t event = 0;
    uint32_t arg = 0;
    switch (index) {
    case SettingsIndexDisplay:
        event = GUI_EVENT_DRAW_DISPLAY_INDEX;
        break;
    case SettingsIndexDisplayVpOffset:
        event = GUI_EVENT_DRAW_DISPLAY_OFFSET;
        break;
    case SettingsIndexDisplayChannelCount:
        event = GUI_EVENT_DRAW_DISPLAY_SIZE;
        break;
    case SettingsIndexDrive:
        event = GUI_EVENT_DRAW_DRIVE_TYPE;
        break;
    case SettingsIndexRemoteGate:
        event = GUI_EVENT_DRAW_GATE_OVERRIDE;
        break;
    case SettingsIndexIsDetached:
        event = GUI_EVENT_DRAW_DETACHED;
        break;
    case SettingsIndexDevAddress:
        event = GUI_EVENT_DRAW_ADDRESS;
        break;
    default:
        return;
    }
    if (focus) {
        event |= GUI_EVENT_DRAW_FOCUS_BIT;
    }
    appletManagerQueueGuiEvent(event, arg);
}

static void appletOnEnter(void)
{
    displayIndex = 0;
    dialHintState = DIAL_HINT_STATE_SEND;
    enterHintState = ENTER_HINT_STATE_NONE;
    pairingRunning = false;
    settingsIndex = 0;
    settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
    queueSettingsControlDraw(settingsIndex, true);
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2)
{
    switch ((enum ProcessEvent)arg1) {
    case PROCESS_EVENT_PROBE_PAIRING:
        probeSuccess = false;
        displayManagerProbePairing(addressUpd);
        if (probeSuccess) {
            pairingRunning = false;
            if (isConfigChanged()) {
                dialHintState = DIAL_HINT_STATE_SAVE;
                enterHintState = ENTER_HINT_STATE_REVERT;
            } else {
                dialHintState = DIAL_HINT_STATE_UNBIND;
                enterHintState = ENTER_HINT_STATE_NONE;
            }
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ADDRESS, 0);
        } 
        if (pairingRunning) {
            appletManagerQueueProcessEvent(PROCESS_EVENT_PROBE_PAIRING, 0);
        }
        break;
    } 
}

static void appletOnGui(uint32_t arg1, uint32_t arg2)
{
    bool inFocus = (arg1 & GUI_EVENT_DRAW_FOCUS_BIT) != 0;
    arg1 = arg1 & (~GUI_EVENT_DRAW_FOCUS_BIT);
    bool drawAll = false;

    switch ((enum GuiEvent)arg1) {
    case GUI_EVENT_DRAW_ALL:
        drawAll = true;
        inFocus = false;
        // fallthrough

    case GUI_EVENT_CLEAR_SCREEN:
        guiElementFillBackground();
        if (!drawAll) {
            break;
        }
        
    case GUI_EVENT_DRAW_DISPLAY_INDEX:
        guiElementLabeledNumericValue(&displayIndex, NumericValueTypeU32, "Display", 
                                      inFocus, NumericBaseDec,  &PosDisplayIndex, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DISPLAY_OFFSET:
        guiElementLabeledNumericValue(&displaySettingsSnapshot.viewportOffset, NumericValueTypeU32, "@", 
                                      inFocus, NumericBaseDec, &PosDisplayOffset, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DISPLAY_SIZE:
        guiElementLabeledNumericValue(&displaySettingsSnapshot.channelCount, NumericValueTypeU32, ":", 
                                      inFocus, NumericBaseDec, &PosDisplaySize, &SizHalfLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DRIVE_TYPE:
        guiElementLabeledString("Drive: ", DisplayDriveTypeStrings[displaySettingsSnapshot.driveType], inFocus, 
                                &PosDriveType, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_GATE_OVERRIDE:
        guiElementCheckBox("Gate override", displaySettingsSnapshot.remoteGate, inFocus, 
                           &PosGateOverride, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DETACHED:
        guiElementCheckBox("Detached", displaySettingsSnapshot.detached, inFocus, 
                          &PosDetached, &SizFullLine);
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_ADDRESS:
        for (uint32_t i = 0; i < DISPLAY_ADDRESS_LENGTH; i++) {
            Point2d position = {PosAddress.x + 22 * i, PosAddress.y};
            guiElementLabeledNumericValue(&displaySettingsSnapshot.deviceAddress[i], NumericValueTypeU8, ":", 
                                          inFocus, NumericBaseHex, &position, &SizThreeChars);
        }
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_DIAL_HINT:
        switch (dialHintState) {
        case DIAL_HINT_STATE_SEND:
            guiElementLabel("o   Send|", false, &PosDialHint, NULL);
            break;
        case DIAL_HINT_STATE_UNBIND:
            guiElementLabel("o Unbind|", false, &PosDialHint, NULL);
            break;
        case DIAL_HINT_STATE_SAVE:
            guiElementLabel("o   Save|", false, &PosDialHint, NULL);
            break;
        }
        if (!drawAll) {
            break;
        }

    case GUI_EVENT_DRAW_ENTER_HINT:
        switch (enterHintState) {
        case ENTER_HINT_STATE_NONE:
            guiElementLabel("K2   Exit", false, &PosEnterHint, NULL);
            break;
        case ENTER_HINT_STATE_ABORT:
            guiElementLabel("K2   Stop", false, &PosEnterHint, NULL);
            break;
        case ENTER_HINT_STATE_REVERT:
            guiElementLabel("K2   Undo", false, &PosEnterHint, NULL);
            break;
        }
        // drawAll fallthrough ends here
        break;

    case GUI_EVENT_DRAW_COMM_ERR_HINT:
        switch (arg2) {
        case 0x00:
            guiElementLabel("COMM: ERROR", true, &PosCommErrHint, NULL);
            break;
        case 0x01:
            guiElementLabel("COMM: OK", true, &PosCommErrHint, NULL);
            break;
        }
        break;
    
    default:
        break;
    }
}

static void onButtonDown(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    bool redrawHints = false;

    if (settingsIndex + 1 < SettingsIndexCount) {
        settingsIndex++;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
        if (prevIndex == SettingsIndexDevAddress && pairingRunning) {
            pairingRunning = false;
            if (isConfigChanged()) {
                dialHintState = DIAL_HINT_STATE_SAVE;
                enterHintState = ENTER_HINT_STATE_REVERT;
            } else {
                dialHintState = DIAL_HINT_STATE_SEND;
                enterHintState = ENTER_HINT_STATE_NONE;
            }
            redrawHints = true;
        }
        if (settingsIndex == SettingsIndexDevAddress) {
            dialHintState = DIAL_HINT_STATE_UNBIND;
            redrawHints = true;
        }
        if (redrawHints) {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        }
    }
}

static void onButtonUp(void)
{
    enum SettingsIndex prevIndex = settingsIndex;
    bool redrawHints = false;

    if (((int32_t)settingsIndex) - 1 >= 0) {
        settingsIndex--;
        queueSettingsControlDraw(prevIndex, false);
        queueSettingsControlDraw(settingsIndex, true);
        if (prevIndex == SettingsIndexDevAddress && pairingRunning) {
            pairingRunning = false;
            if (isConfigChanged()) {
                dialHintState = DIAL_HINT_STATE_SAVE;
                enterHintState = ENTER_HINT_STATE_REVERT;
            } else {
                dialHintState = DIAL_HINT_STATE_SEND;
                enterHintState = ENTER_HINT_STATE_NONE;
            }
        }
        if (settingsIndex == SettingsIndexDevAddress) {
            dialHintState = DIAL_HINT_STATE_UNBIND;
            redrawHints = true;
        }
        if (redrawHints) {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        }
    }
}

static void onButtonDial(void)
{
    bool redrawHints = false;

    switch (dialHintState) {
    case DIAL_HINT_STATE_UNBIND:
        // start pairing
        pairingRunning = true;
        enterHintState = ENTER_HINT_STATE_ABORT;
        redrawHints = true;
        appletManagerQueueProcessEvent(PROCESS_EVENT_PROBE_PAIRING, 0);
        break;
    case DIAL_HINT_STATE_SAVE:
        // save settings
        settingsSetDisplay(displayIndex, &displaySettingsSnapshot);
        dialHintState = DIAL_HINT_STATE_SEND;
        enterHintState = ENTER_HINT_STATE_NONE;
        redrawHints = true;
        break;
    case DIAL_HINT_STATE_SEND:
        // send settings
        if (sendSettings()) {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_COMM_ERR_HINT, 0x01);
        } else {
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_COMM_ERR_HINT, 0x00);
        }
        break;
    }
    if (redrawHints) {
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
    }
}

static void onButtonEnter(void)
{
    bool redrawHints = false;

    switch (enterHintState) {
    case ENTER_HINT_STATE_REVERT:
        // revert settings
        settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
        enterHintState = ENTER_HINT_STATE_NONE;
        dialHintState = DIAL_HINT_STATE_SEND;
        redrawHints = true;
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        queueSettingsControlDraw(settingsIndex, true);
        break;
    case ENTER_HINT_STATE_NONE:
        // exit applet
        appletManagerQueueAppletExit();
        break;
    case ENTER_HINT_STATE_ABORT:
        // abort pairing
        dialHintState = DIAL_HINT_STATE_UNBIND;
        enterHintState = ENTER_HINT_STATE_NONE;
        pairingRunning = false;
        redrawHints = true;
        break;
    }

    if (redrawHints) {
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
    }
}

static void onEncoder(int32_t delta)
{
    bool recheckConfig = false;
    switch (settingsIndex) {

    case SettingsIndexDisplay:
        if (((int32_t)displayIndex) + delta < DISPLAY_COUNT
            && ((int32_t)displayIndex) + delta >= 0) {
            displayIndex += delta;
            enterHintState = ENTER_HINT_STATE_NONE;
            dialHintState = DIAL_HINT_STATE_SEND;
            settingsGetDisplay(displayIndex, &displaySettingsSnapshot);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
            // is this necessary?
            recheckConfig = true;
        }
        break;

    case SettingsIndexDisplayVpOffset:
        if ((int32_t)(displaySettingsSnapshot.channelCount + displaySettingsSnapshot.viewportOffset) + delta <= VIEWPORT_POOL_SIZE 
            && (int32_t)(displaySettingsSnapshot.viewportOffset + displaySettingsSnapshot.channelCount) + delta >= 0   
            && (int32_t)displaySettingsSnapshot.viewportOffset + delta >= 0) {
            displaySettingsSnapshot.viewportOffset += delta;
            recheckConfig = true;
        }
        break;

    case SettingsIndexDisplayChannelCount:
        if ((int32_t)(displaySettingsSnapshot.channelCount + displaySettingsSnapshot.viewportOffset) + delta <= VIEWPORT_POOL_SIZE 
            && (int32_t)(displaySettingsSnapshot.viewportOffset + displaySettingsSnapshot.channelCount) + delta >= 0         
            && (int32_t)displaySettingsSnapshot.channelCount + delta >= 0) {
            displaySettingsSnapshot.channelCount += delta;
            recheckConfig = true;
        }
        break;

    case SettingsIndexDrive:
        if ((int32_t)displaySettingsSnapshot.driveType + delta < 0)
            displaySettingsSnapshot.driveType = 0;
        else if ((int32_t)displaySettingsSnapshot.driveType + delta >= DriveTypeCount )
            displaySettingsSnapshot.driveType = DriveTypeCount - 1;
        else {
            displaySettingsSnapshot.driveType += delta;
            recheckConfig = true;
        }
        break;

    case SettingsIndexRemoteGate:
        if (delta > 0)
            displaySettingsSnapshot.remoteGate = true;
        else if (delta < 0)
            displaySettingsSnapshot.remoteGate = false;
        recheckConfig = true;
        break;

    case SettingsIndexIsDetached:
        if (delta > 0)
            displaySettingsSnapshot.detached = true;
        else if (delta < 0)
            displaySettingsSnapshot.detached = false;
        recheckConfig = true;
        break;

    default:
        return;
    }

    if (recheckConfig) {
        queueSettingsControlDraw(settingsIndex, true);
        if (isConfigChanged()) {
            dialHintState = DIAL_HINT_STATE_SAVE;
            enterHintState = ENTER_HINT_STATE_REVERT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        } else {
            if (dialHintState != DIAL_HINT_STATE_SEND) {
                dialHintState = DIAL_HINT_STATE_SEND;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_DIAL_HINT, 0);
            }
            if (enterHintState != ENTER_HINT_STATE_NONE) {
                enterHintState = ENTER_HINT_STATE_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            }
        }
    }
}

static void appletOnInput(InputEvent evt)
{
    switch (evt.type) {
    case EventButtonDown:
        if (evt.buttonState) {
            onButtonDown();
        } 
        break;
    case EventButtonUp:
        if (evt.buttonState) {
            onButtonUp();
        }
        break;
    case EventButtonDial:
        if (evt.buttonState) {
            onButtonDial();
        }
        break;
    case EventButtonEnter:
        if (evt.buttonState) {
            onButtonEnter();
        }
        break;
    case EventEncoder:
        onEncoder(evt.encoderDelta);
        break;
    }
}

static void appletOnExit(void)
{

}

void displaySettingsStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}