#include "hostConnection.h"
#include "appletManager.h"
#include "guiElements.h"
#include "settings.h"
#include "common.h"
#include "displayManager.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>

#define GLOBAL_X_OFFSET (4U)
#define HOST_DATA_BUFFER_LEN (64U)

enum GuiCommand {
    GUI_COMMAND_SET_BACKGROUND,
    GUI_COMMAND_DRAW_STATUS_CONNECTED,
    GUI_COMMAND_DRAW_PROTOCOL,
};

typedef enum {
    PROTOCOL_GENERIC_SERIAL,
    PROTOCOL_RENARD,
    PROTOCOL_COUNT
} Protocol;

static const char TextConnected[] = "Host connected"; 
static const char TextProtocol[] = "Protocol:"; 
static const char *ProtocolString[PROTOCOL_COUNT] = {
    [PROTOCOL_GENERIC_SERIAL] = "Serial",
    [PROTOCOL_RENARD] = "Renard"
};
static const Point2d PosConnected = {GLOBAL_X_OFFSET, 2};
static const Point2d PosProtocol = {GLOBAL_X_OFFSET, 14};
static const Point2d SizFullLine = {128, 12};
static Protocol currentProtocol = PROTOCOL_RENARD;

static void appletOnEnter(void)
{
    appletManagerQueueGuiEvent(GUI_COMMAND_SET_BACKGROUND, 0);
    appletManagerQueueGuiEvent(GUI_COMMAND_DRAW_STATUS_CONNECTED, 0);
    appletManagerQueueGuiEvent(GUI_COMMAND_DRAW_PROTOCOL, 0);
}

static void appletOnProcess(uint32_t arg1, uint32_t arg2)
{

}

static void appletOnGui(uint32_t arg1, uint32_t arg2)
{
    switch ((enum GuiCommand)arg1) {
    case GUI_COMMAND_SET_BACKGROUND:
        guiElementFillBackground();
        break;
    case GUI_COMMAND_DRAW_STATUS_CONNECTED:
        guiElementLabel(TextConnected, 
                        true, 
                        &PosConnected, 
                        &SizFullLine);
        break;
    case GUI_COMMAND_DRAW_PROTOCOL:
        guiElementLabeledString(TextProtocol, 
                                ProtocolString[currentProtocol], 
                                false, 
                                &PosProtocol, 
                                &SizFullLine);
        break;
    }
}

static void appletOnInput(InputEvent evt)
{

}

static void appletOnExit(void)
{

}

static void appletOnHostData(AppletHostDataType type, const uint8_t *data, size_t len)
{
    if (len == 0 || data == NULL) {
        return;
    }

    switch (type) {
    case APPLET_HOST_DATA_TYPE_CHANNEL:
        displayManagerUpdateChannelData(data, len);
        break;
    default:
        break;
    }
}

void hostConnectionStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = appletOnProcess,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = appletOnHostData,
        .onExit = appletOnExit
    };
    appletManagerStartApplet(&cb);
}

void hostConnectionStop(void)
{
    appletManagerQueueAppletExit();
}