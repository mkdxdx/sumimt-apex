#pragma once
#include <stdint.h>

typedef struct RgbColor
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
} RgbColor;

typedef struct HsvColor
{
    uint8_t h;
    uint8_t s;
    uint8_t v;
} HsvColor;


void HsvToRgb(HsvColor * hsv, RgbColor * rgb);
void RgbToHsv(RgbColor * rgb, HsvColor * hsv);
