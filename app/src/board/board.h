#pragma once

#include <stdbool.h>
#include <stdint.h>

#define BOARD_CHIP_ID_LEN (16U)
#define BOARD_SECTOR_SIZE (4096U)

typedef enum {
    BOARD_PIN_BUTTON_DIAL,
    BOARD_PIN_BUTTON_DOWN,
    BOARD_PIN_BUTTON_ENTER,
    BOARD_PIN_BUTTON_UP,
    BOARD_PIN_COUNT
} BoardPin;

typedef enum {
    BOARD_ENCODER_CHANGE_INCREMENT,
    BOARD_ENCODER_CHANGE_DECREMENT
} BoardEncoderChange;

typedef void (*BoardEncoderCallback) (BoardEncoderChange change);

void boardInit(void);
void boardGetChipUid(uint8_t data[BOARD_CHIP_ID_LEN]);
void boardSetPin(BoardPin pin, bool state);
bool boardGetPin(BoardPin pin);
void boardFlashPageWrite(uint32_t address, uint8_t data[BOARD_SECTOR_SIZE]);
void boardSetEncoderChangeCb(BoardEncoderCallback encoderChangeCb);
