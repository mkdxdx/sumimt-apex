#include "nrf_gpio.h"
#include "nrfx_spim.h"
#include "nrfx_qdec.h"
#include "nrf_drv_clock.h"
#include "nrf.h"
#include "board.h"
#include <string.h>
#include <stdint.h>
#include <assert.h>

// LCD interface definitions
#define PIN_LCD_MOSI        NRF_GPIO_PIN_MAP(0, 20)
#define PIN_LCD_SCK         NRF_GPIO_PIN_MAP(0, 21)
#define PIN_LCD_CS          NRF_GPIO_PIN_MAP(0, 17)
#define PIN_LCD_A0          NRF_GPIO_PIN_MAP(0, 19)
#define PIN_LCD_RES         NRF_GPIO_PIN_MAP(0, 15)

// encoder definitions
#define PIN_ENCODER_CLK     NRF_GPIO_PIN_MAP(0, 28)
#define PIN_ENCODER_DT      NRF_GPIO_PIN_MAP(0, 5)

#define PIN_BUTTON_DIAL     NRF_GPIO_PIN_MAP(0, 4)
#define PIN_BUTTON_UP       NRF_GPIO_PIN_MAP(0, 29)
#define PIN_BUTTON_ENTER    NRF_GPIO_PIN_MAP(0, 30)
#define PIN_BUTTON_DOWN     NRF_GPIO_PIN_MAP(0, 31)

/*
#define FLASH_BASE_ADDRESS   0x08000000
#define FLASH_PAGE_SIZE      1024
#define FLASH_CFG_PAGE       63
#define FLASH_PAGE_COUNT     64
#define FLASH_SIZE           (FLASH_PAGE_COUNT * FLASH_PAGE_SIZE)
#define FLASH_CFG_PAGE_COUNT 1
#define FLASH_CFG_SIZE       (FLASH_PAGE_SIZE * FLASH_CFG_PAGE_COUNT)
#define FLASH_CFG_OFFSET     (FLASH_CFG_PAGE * FLASH_PAGE_SIZE)
#define FLASH_CFG_ADDRESS    (FLASH_CFG_OFFSET + FLASH_BASE_ADDRESS)
#define FLASH_CFG_PAGE_COUNT 1
*/

typedef struct BoardGpio {
    uint32_t gpio;
} BoardGpio;

static const BoardGpio BoardPins[BOARD_PIN_COUNT] = {
    [BOARD_PIN_BUTTON_DIAL]       = { .gpio = PIN_BUTTON_DIAL },
    [BOARD_PIN_BUTTON_DOWN]       = { .gpio = PIN_BUTTON_DOWN },
    [BOARD_PIN_BUTTON_ENTER]      = { .gpio = PIN_BUTTON_ENTER },
    [BOARD_PIN_BUTTON_UP]         = { .gpio = PIN_BUTTON_UP },
};

static BoardEncoderCallback encoderCb = NULL;
static volatile int16_t encoderRot = 0;

static void gpioInit(void)
{
    nrf_gpio_cfg_input(PIN_BUTTON_DIAL, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(PIN_BUTTON_DOWN, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(PIN_BUTTON_ENTER, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(PIN_BUTTON_UP, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(PIN_ENCODER_DT, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(PIN_ENCODER_CLK, NRF_GPIO_PIN_PULLUP);
}

static void encoderHandler(nrfx_qdec_event_t evt)
{
    static volatile int16_t pcnt = 0;
    if (evt.type == NRF_QDEC_EVENT_REPORTRDY) {
        pcnt += evt.data.report.acc;
        if (pcnt % 2 == 0) {
            int16_t newRot = encoderRot + pcnt / 2;
            if (encoderCb) {
                if (newRot < encoderRot) {
                    encoderCb(BOARD_ENCODER_CHANGE_INCREMENT);
                }
                if (newRot > encoderRot) {
                    encoderCb(BOARD_ENCODER_CHANGE_DECREMENT);
                }
            }
            encoderRot = newRot;
            pcnt = 0;
        }
    }
}

static void encoderInit() 
{
    static nrfx_qdec_config_t cfg = NRFX_QDEC_DEFAULT_CONFIG;
    cfg.psela = PIN_ENCODER_DT;
    cfg.pselb = PIN_ENCODER_CLK;
    cfg.pselled = NRF_QDEC_LED_NOT_CONNECTED;
    assert(nrfx_qdec_init(&cfg, encoderHandler) == NRFX_SUCCESS);
    nrfx_qdec_enable();
}

static void usbInit(void)
{
    
}

static void spiInit(void)
{

}

static void dmaInit(void)
{
    
}

static void extiInit(void)
{
    
}

static void flashInit(void)
{
    
}

static inline void waitRdy(void)
{
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {;}
}

void boardInit(void) {

    ret_code_t ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    nrf_drv_clock_hfclk_request(NULL);
    while (!nrf_drv_clock_hfclk_is_running());

    nrf_drv_clock_lfclk_request(NULL);
    while (!nrf_drv_clock_lfclk_is_running());

    gpioInit();
    encoderInit();
}

void boardSetPin(BoardPin pin, bool state)
{
    if (pin >= BOARD_PIN_COUNT)
        return;

    if (state) {
        nrf_gpio_pin_set(BoardPins[pin].gpio);
    } else {
        nrf_gpio_pin_clear(BoardPins[pin].gpio);
    }

}

bool boardGetPin(BoardPin pin)
{
    if (pin >= BOARD_PIN_COUNT)
        return false;

    return nrf_gpio_pin_read(BoardPins[pin].gpio) > 0;
}

void boardGetChipUid(uint8_t data[BOARD_CHIP_ID_LEN])
{
    volatile uint32_t *idAddr = NRF_FICR->DEVICEID;
    memcpy(data, idAddr, BOARD_CHIP_ID_LEN);
}

void boardFlashPageWrite(uint32_t address, uint8_t data[BOARD_SECTOR_SIZE])
{
    // erase enable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Een;
    __ISB();
    __DSB();

    // erase
    NRF_NVMC->ERASEPAGE = address;
    waitRdy();

    // write enable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;
    __ISB();
    __DSB();

    // write
    uint32_t *src = (uint32_t *)data;
    for (uint32_t i = 0; i < BOARD_SECTOR_SIZE / sizeof(uint32_t); i++) {
        ((uint32_t*)address)[i] = src[i];
        waitRdy();
    }

    // write disable
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;
    __ISB();
    __DSB();
}

void boardSetEncoderChangeCb(BoardEncoderCallback encoderChangeCb)
{
    __disable_irq();
    encoderCb = encoderChangeCb;
    __enable_irq();
}