#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "appletTypes.h"

void appletManagerStart(void);
void appletManagerStartApplet(AppletCallbacks *cb);
void appletManagerQueueGuiEvent(uint32_t arg1, uint32_t arg2);
void appletManagerQueueProcessEvent(uint32_t arg1, uint32_t arg2);
void appletManagerQueueAppletExit(void);
void appletManagerSleep(uint32_t timeMs);
void *appletManagerRequestMemory(size_t size);
void appletManagerFreeMemory(void *ptr);
void appletManagerStartTimer(uint32_t timeoutMs, void (*callback)(void));
