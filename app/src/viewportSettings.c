#include "appletManager.h"
#include "viewportSettings.h"
#include "settings.h"
#include "common.h"
#include "guiElements.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define GLOBAL_X_OFFSET (4U)

enum GuiEvent {
    GUI_EVENT_DRAW_ALL,
    GUI_EVENT_DRAW_VIEWPORT_INDEX,
    GUI_EVENT_DRAW_VIEWPORT_OFFSET,
    GUI_EVENT_DRAW_VIEWPORT_CHANNEL_COUNT,
    GUI_EVENT_DRAW_DIAL_HINT,
    GUI_EVENT_DRAW_ENTER_HINT,

    GUI_EVENT_DRAW_FOCUS_BIT = 0x80U,
};

static const Point2d PosViewportIndex = {GLOBAL_X_OFFSET, 2};
static const Point2d PosViewportOffset = {GLOBAL_X_OFFSET, 14};
static const Point2d PosViewportChannelCount = {GLOBAL_X_OFFSET + 64, 14};
static const Point2d PosDialHint = {GLOBAL_X_OFFSET, 116};
static const Point2d PosEnterHint = {GLOBAL_X_OFFSET + 64, 116};

static const Point2d SizHalfLine = {64, 12};
static const Point2d SizFullLine = {128, 12};

static ViewportSettings viewportSettingsSnapshot;
static uint32_t viewportIndex = 0;
static enum {
    ENTER_STATE_HINT_NONE,
    ENTER_STATE_HINT_REVERT
} enterStateHint = ENTER_STATE_HINT_NONE;
static enum SettingsIndex {
    SettingsIndexViewportIndex,
    SettingsIndexViewportOffset,
    SettingsIndexViewportChannelCount,
    SettingsIndexCount
} settingsIndex = SettingsIndexViewportIndex;

static bool isConfigChanged(void)
{
    ViewportSettings settings;
    settingsGetViewport(viewportIndex, &settings);
    return 0 != memcmp(&settings, &viewportSettingsSnapshot, sizeof(settings));
}

static void queueSettingsControlDraw(enum SettingsIndex index, bool focus)
{
    uint32_t event = 0;
    uint32_t arg = 0;
    switch (index) {
    case SettingsIndexViewportIndex:
        event = GUI_EVENT_DRAW_VIEWPORT_INDEX;
        break;
    case SettingsIndexViewportOffset:
        event = GUI_EVENT_DRAW_VIEWPORT_OFFSET;
        break;
    case SettingsIndexViewportChannelCount:
        event = GUI_EVENT_DRAW_VIEWPORT_CHANNEL_COUNT;
        break;
    default:
        return;
    }
    if (focus) {
        event |= GUI_EVENT_DRAW_FOCUS_BIT;
    }
    appletManagerQueueGuiEvent(event, arg);
}

static void appletOnEnter(void)
{
    enterStateHint = ENTER_STATE_HINT_NONE;
    viewportIndex = 0;
    settingsIndex = SettingsIndexViewportIndex;
    settingsGetViewport(viewportIndex, &viewportSettingsSnapshot);
    appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
    queueSettingsControlDraw(settingsIndex, true);
}

static void appletOnGui(uint32_t arg1, uint32_t arg2)
{
    bool inFocus = (arg1 & GUI_EVENT_DRAW_FOCUS_BIT) != 0;
    arg1 = arg1 & (~GUI_EVENT_DRAW_FOCUS_BIT);
    bool drawAll = false;

    ViewportSettings *viewport = &viewportSettingsSnapshot; 

    switch ((enum GuiEvent)arg1) {
    case GUI_EVENT_DRAW_ALL:
        drawAll = true;
        inFocus = false;
        guiElementFillBackground();

    case GUI_EVENT_DRAW_VIEWPORT_INDEX:
        guiElementLabeledNumericValue(&viewportIndex, NumericValueTypeU32, "Viewport:", 
                                      inFocus, NumericBaseDec, &PosViewportIndex, &SizFullLine);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_VIEWPORT_OFFSET:
        guiElementLabeledNumericValue(&viewport->offset, NumericValueTypeU32, "@", 
                                      inFocus, NumericBaseDec, &PosViewportOffset, &SizHalfLine);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_VIEWPORT_CHANNEL_COUNT:
        guiElementLabeledNumericValue(&viewport->size, NumericValueTypeU32, "#", 
                                      inFocus, NumericBaseDec, &PosViewportChannelCount, &SizHalfLine);
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_ENTER_HINT:
        switch (enterStateHint) {
        case ENTER_STATE_HINT_NONE:
            guiElementLabel("K2   Exit", false, &PosEnterHint, NULL);
            break;
        case ENTER_STATE_HINT_REVERT:
            guiElementLabel("K2 Revert", false, &PosEnterHint, NULL);
            break;
        }
        if (!drawAll) {
            break;
        }
    case GUI_EVENT_DRAW_DIAL_HINT:
        guiElementLabel("o   Save|", false, &PosDialHint, NULL);
        break;
    }
}

static void onButtonDown(void)
{
    enum SettingsIndex prevSettings = settingsIndex;
    if (settingsIndex + 1 < SettingsIndexCount) {
        settingsIndex++;
        queueSettingsControlDraw(prevSettings, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonUp(void)
{
    enum SettingsIndex prevSettings = settingsIndex;
    if (settingsIndex - 1 >= 0) {
        settingsIndex--;
        queueSettingsControlDraw(prevSettings, false);
        queueSettingsControlDraw(settingsIndex, true);
    }
}

static void onButtonEnter(void)
{
    switch (enterStateHint) {
    case ENTER_STATE_HINT_NONE:
        appletManagerQueueAppletExit();
        break;
    case ENTER_STATE_HINT_REVERT:
        enterStateHint = ENTER_STATE_HINT_NONE;
        settingsGetViewport(viewportIndex, &viewportSettingsSnapshot);
        appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
        break;
    }
}

static void onEncoder(int32_t delta)
{
    bool recheckConfig = false;
    bool redrawControl = false;

    ViewportSettings *viewport = &viewportSettingsSnapshot;

    switch (settingsIndex) {
    case SettingsIndexViewportIndex:
        if ((int32_t)viewportIndex + delta < VIEWPORT_COUNT
            && (int32_t)viewportIndex + delta >= 0) {
            viewportIndex += delta;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ALL, 0);
            redrawControl = true;
        }
        break;
    case SettingsIndexViewportOffset:
        if ((int32_t)(viewport->offset + viewport->size) + delta < VIEWPORT_POOL_SIZE
            && (int32_t)viewport->offset + delta >= 0) {
            viewport->offset += delta;
            redrawControl = true;
            recheckConfig = true;
        } 
        break;
    case SettingsIndexViewportChannelCount:
        if ((int32_t)(viewport->offset + viewport->size) + delta < VIEWPORT_POOL_SIZE
            && (int32_t)viewport->size >= 1) {
            viewport->size += delta;
            redrawControl = true;
            recheckConfig = true;
        }
        break;
    }

    if (redrawControl) {
        queueSettingsControlDraw(settingsIndex, true);
    }

    if (recheckConfig) {
        if (isConfigChanged()) {
            enterStateHint = ENTER_STATE_HINT_REVERT;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        } else {
            if (enterStateHint != ENTER_STATE_HINT_NONE) {
                enterStateHint = ENTER_STATE_HINT_NONE;
                appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
            }
        }
    }

}

static void appletOnInput(InputEvent evt)
{
    switch (evt.type) {
    case EventButtonDown:
        if (evt.buttonState) {
            onButtonDown();
        } 
        break;
    case EventButtonUp:
        if (evt.buttonState) {
            onButtonUp();
        }
        break;
    case EventButtonDial:
        if (evt.buttonState) {
            settingsSetViewport(viewportIndex, &viewportSettingsSnapshot);
            enterStateHint = ENTER_STATE_HINT_NONE;
            appletManagerQueueGuiEvent(GUI_EVENT_DRAW_ENTER_HINT, 0);
        }
        break;
    case EventButtonEnter:
        if (evt.buttonState) {
            onButtonEnter();
        }
        break;
    case EventEncoder:
        onEncoder(evt.encoderDelta);
        break;
    }
}

void viewportSettingsStart(void)
{
    AppletCallbacks cb = {
        .onEnter = appletOnEnter,
        .onProcess = NULL,
        .onGui = appletOnGui,
        .onInput = appletOnInput,
        .onHostDataEvent = NULL,
        .onExit = NULL
    };
    appletManagerStartApplet(&cb);
}