#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef enum DisplayStaticEffectType {
    DisplayStaticEffectTypeNone,
    DisplayStaticEffectTypeFill,
    DisplayStaticEffectTypeSwitch,
    DisplayStaticEffectTypeChase,
    DisplayStaticEffectTypeFade,
    DisplayStaticEffectTypeRainbow,
    DisplayStaticEffectTypeCount
} DisplayStaticEffectType;

typedef enum DisplayDriverType {
    DisplayDriverTypeNone,
    DisplayDriverTypePWM,
    DisplayDriverTypePixel1,
    DisplayDriverTypePixel2,
    DisplayDriverTypeSerial1,
    DisplayDriverTypeSerial2,
    DisplayDriverTypeCount,
} DisplayDriverType;

typedef enum  {
    DisplayManagerModeNormal,
    DisplayManagerModePairing,
    DisplayManagerModeCount
} DisplayManagerMode;

typedef struct DisplayInfo {
    enum {
        SignalNone,
        SignalLow,
        SignalMedium,
        SignalHigh,
    }                  signal;
} DisplayInfo;

typedef struct DisplayConfig {
    uint32_t          channelCount;
    DisplayDriverType driverType;
    bool              gateOverride;
} DisplayConfig;

typedef struct DisplayStaticEffectColor {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} DisplayStaticEffectColor;

typedef struct DisplayStaticEffect {
    DisplayStaticEffectType type;
    uint32_t periodMs;
    bool lateral;
    bool mirror;
    bool reverse;
    uint32_t paletteColorCount;
    uint32_t paletteColorIndex;
    DisplayStaticEffectColor *paletteColor;
} DisplayStaticEffect;

typedef void (*DisplayManagerPresenceReceivedCallback) (const uint8_t *data, uint32_t size);

void displayManagerInit(void);
void displayManagerUpdateChannelData(const uint8_t *data, uint32_t size);
bool displayManagerSendConfig(uint32_t displayIndex, const DisplayConfig *config);
bool displayManagerSetStaticEffect(uint32_t displayIndex, const DisplayStaticEffect *effect);
bool displayManagerGetInfo(uint32_t displayIndex, DisplayInfo *info);
void displayManagerProbePairing(DisplayManagerPresenceReceivedCallback cb);