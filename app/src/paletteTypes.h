#pragma once

#include "color.h"

#define PALETTE_SIZE    8

typedef enum PaletteType {
    PaletteTypeRGB,
    PaletteTypeMono
} PaletteType;

typedef struct Palette {
    unsigned int colorCount;
    PaletteType type;
    union {
        unsigned char intensities[PALETTE_SIZE];
        RgbColor      color[PALETTE_SIZE];
    };
} Palette;
