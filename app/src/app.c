#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "board.h"
#include "color.h"
#include "app.h"
#include "common.h"
#include "config.h"
#include "settings.h"
#include "guiElements.h"
#include "displayManager.h"
#include "appletManager.h"
#include "mainMenu.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// #include "core_cm4.h"

#define HOST_PKT_SIZE 63
#define HOST_PKT_QUEUE_COUNT 3

static void * hostPhospheneMemReq(uint32_t size, void * userdata);
static void   hostPhospheneMemRel(void * ptr, void * userdata);
static bool   hostPhospheneHunkReady(const uint8_t * data, uint32_t length, void * userdata);

static SemaphoreHandle_t lcdSemaphore = NULL;

static inline bool isInterrupt(void)
{
    return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0 ;
}

static SemaphoreHandle_t spiMutex = NULL;
static SemaphoreHandle_t spiSemaphore = NULL;
static SemaphoreHandle_t flashMutex = NULL;

static void settingsLockInit(void)
{
    flashMutex = xSemaphoreCreateMutex();
}

void settingsLock(bool lock)
{
    if (lock)
        xSemaphoreTake(flashMutex, portMAX_DELAY);
    else
        xSemaphoreGive(flashMutex);
}

#include "nrfx_systick.h"

static uint32_t logTs(void)
{
    nrfx_systick_state_t state;
    nrfx_systick_get(&state);
    return state.time;
} 

void appStart() {
    boardInit();
    settingsLockInit();
    settingsInit(settingsLock);

    APP_ERROR_CHECK(NRF_LOG_INIT(logTs));
    NRF_LOG_DEFAULT_BACKENDS_INIT();    

    vTaskStartScheduler();
    for( ;; );
}

void vApplicationDaemonTaskStartupHook( void )
{
    if (!boardGetPin(BOARD_PIN_BUTTON_DIAL))
        settingsSetDefault();

    displayManagerInit();
    appletManagerStart();
    mainMenuStart();
}

/* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
                                    StackType_t **ppxIdleTaskStackBuffer,
                                    uint32_t *pulIdleTaskStackSize )
{
/* If the buffers to be provided to the Idle task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
static StaticTask_t xIdleTaskTCB;
static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
application must provide an implementation of vApplicationGetTimerTaskMemory()
to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
                                     StackType_t **ppxTimerTaskStackBuffer,
                                     uint32_t *pulTimerTaskStackSize )
{
/* If the buffers to be provided to the Timer task are declared inside this
function then they must be declared static - otherwise they will be allocated on
the stack and so not exists after this function exits. */
static StaticTask_t xTimerTaskTCB;
static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

    /* Pass out a pointer to the StaticTask_t structure in which the Timer
    task's state will be stored. */
    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

    /* Pass out the array that will be used as the Timer task's stack. */
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;

    /* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

void vApplicationMallocFailedHook( void )
{
    __asm__ volatile ("BKPT");
    __asm__ volatile ("NOP");
}

void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName )
{
    (void)xTask;
    (void)pcTaskName;
    __asm__ volatile ("BKPT");
    __asm__ volatile ("NOP");
}

void lcdSpiOsTransferWait(void)
{
    assert(lcdSemaphore != NULL);
    assert(xSemaphoreTake(lcdSemaphore, pdMS_TO_TICKS(1000)) == pdTRUE);
}

void lcdSpiOsTransferDone(void)
{
    assert(lcdSemaphore != NULL);
    BaseType_t yield = pdFALSE;
    xSemaphoreGiveFromISR(lcdSemaphore, &yield);
    portYIELD_FROM_ISR(yield);
}

void lcdSpiOsTransferInit(void)
{
    lcdSemaphore = xSemaphoreCreateBinary();
    assert(lcdSemaphore != NULL);
}

void lcdSpiOsTransferUninit(void)
{
    vSemaphoreDelete(lcdSemaphore);
    lcdSemaphore = NULL;
}

void lcdOsDelayMs(unsigned int ms)
{
    vTaskDelay(pdMS_TO_TICKS(ms));
}