#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>
#include "displayManager.h"
#include "common.h"
#include "config.h"
#include "board.h"
#include "protocol.h"
#include "settings.h"
#include "effectTypes.h"
#include "displayMode.h"
#include "paletteTypes.h"

#include "radio.h"

#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#include "event_groups.h"
#include "semphr.h"

#define MAX_DEVICES 5
#define ADDR_LENGTH (ADDRESS_LENGTH)
#define RF_PKT_PAYLOAD_LENGTH (RADIO_DATA_MAX_LENGTH)
#define ACK_TIMEOUT_MS (10U)
#define PAIRING_HEADER (0xFFU)

static void rfSentCb(bool success);
static void rfReceivedCb(const uint8_t *data, size_t size);
static void rfInit(void);

static void rfSetTxAddress(const uint8_t address[ADDR_LENGTH]);
static bool sendStaticEffectData(const DisplayStaticEffect *effect);
static void sendChannelData(const uint8_t *data, uint32_t size);
static bool sendConfigData(const DisplayConfig *config);
static bool sendHunk(uint8_t *data, size_t size);

static const uint8_t UnboundTxAddress[5] = {0xE7, 0xE7, 0xE7, 0xE7, 0xE7};
static const uint8_t UnboundRxAddress[5] = {0x7E, 0x7E, 0x7E, 0x7E, 0x7E};
static const uint32_t DefaultCommChannel = 15;

static SemaphoreHandle_t txSemaphore = NULL;
static SemaphoreHandle_t rxSemaphore = NULL;
static SemaphoreHandle_t radioMutex = NULL;

static volatile bool txSuccess = false;

static uint8_t rxData[RADIO_DATA_MAX_LENGTH];
static uint32_t rxDataLen = 0;

void displayManagerInit(void)
{
    txSemaphore = xSemaphoreCreateBinary();
    rxSemaphore = xSemaphoreCreateBinary();
    radioMutex = xSemaphoreCreateMutex();
    rfInit();
}

void displayManagerUpdateChannelData(const uint8_t *data, uint32_t size)
{
    if (data == NULL || size == 0)
        return;

    DisplaySettings sdisp;
    for (uint32_t i = 0; i < DISPLAY_COUNT; i++) {
        settingsGetDisplay(i, &sdisp);
        if (sdisp.channelCount == 0 || sdisp.viewportOffset > size || sdisp.detached)
            continue;

        rfSetTxAddress(sdisp.deviceAddress);        
        sendChannelData(&data[sdisp.viewportOffset], sdisp.channelCount);
    }
}

bool displayManagerSendConfig(uint32_t displayIndex, const DisplayConfig *config)
{
    if (config == NULL)
        return false;

    DisplaySettings sdisp;
    settingsGetDisplay(displayIndex, &sdisp);
    rfSetTxAddress(sdisp.deviceAddress);
    return sendConfigData(config);
}

bool displayManagerGetInfo(uint32_t displayIndex, DisplayInfo *info)
{
    (void)displayIndex;
    (void)info;
    return false;
}

bool displayManagerSetStaticEffect(uint32_t displayIndex, const DisplayStaticEffect *effect)
{
    if (effect == NULL)
        return false;

    DisplaySettings sdisp;
    settingsGetDisplay(displayIndex, &sdisp);
    rfSetTxAddress(sdisp.deviceAddress);
    return sendStaticEffectData(effect);
}

void displayManagerProbePairing(DisplayManagerPresenceReceivedCallback cb)
{
    // setup probe data
    union {
        struct {
            uint8_t probeHeader;
            uint8_t addressLen;
            uint8_t address[ADDRESS_LENGTH];
        };
        uint8_t data[2 + ADDRESS_LENGTH];
    } probeData = {
        .probeHeader = PAIRING_HEADER,
        .addressLen = ADDRESS_LENGTH
    };
    uint8_t tempId[BOARD_CHIP_ID_LEN];
    boardGetChipUid(tempId);
    memcpy(probeData.address, tempId, ADDRESS_LENGTH);
    // set tx unbound address
    radioSetTxAddress(UnboundTxAddress);
    // send probe, wait for transfer success
    xSemaphoreTake(radioMutex, portMAX_DELAY);
    txSuccess = false;
    rxDataLen = 0;
    rxData[0] = 0;
    radioSendData(probeData.data, sizeof(probeData.data));
    xSemaphoreTake(txSemaphore, portMAX_DELAY);
    if (txSuccess) {
        // if send ok, wait for ack
        if (xSemaphoreTake(rxSemaphore, pdMS_TO_TICKS(ACK_TIMEOUT_MS)) == pdTRUE) {
            // if got ack, check for contents and call callback
            if (rxDataLen > 0 && rxData[0] == PAIRING_HEADER && cb) {
                cb(&rxData[2], rxDataLen - 2);
            }           
        }
    }
    xSemaphoreGive(radioMutex);
}

static bool slavePhospheneHunkReady(const uint8_t * data, uint32_t length, void * userData)
{
    (void)length;
    (void)userData;

    DataHeader * header = (DataHeader *)data;
    if (header->dataId == DataIdConfigPairList) {
        ConfigPairListHeader * listHeader = (ConfigPairListHeader *)((uint8_t *)header + sizeof(DataHeader));
        ConfigPair *           pair = (ConfigPair *)((uint8_t *)listHeader + sizeof(ConfigPairListHeader));
        for (uint32_t i = 0; i < listHeader->configPairCount; i++) {
            switch (pair[i].configKey) {
                case ConfigKeyCommChannel:
                    break;
                default:
                    break;
            }
        }
    }
    return true;
}

static void rfSetTxAddress(const uint8_t address[ADDR_LENGTH])
{
    xSemaphoreTake(radioMutex, portMAX_DELAY);
    radioSetTxAddress(address);
    xSemaphoreGive(radioMutex);
}  

static void sendChannelData(const uint8_t *data, uint32_t size)
{
    if (data == NULL || size == 0)
        return;

    uint32_t  bufferSize = PROTOCOL_CHANNEL_DATA_SIZE(size);
    uint8_t * buffer     = (uint8_t *)pvPortMalloc(bufferSize);
    if (buffer == NULL)
        return;

    memset(buffer, 0, bufferSize);
    DataHeader * header = (DataHeader *)buffer;
    header->dataId      = DataIdChannelData;
    ChannelDataHeader * channelData = (ChannelDataHeader *)(((uint8_t *)header) + sizeof(DataHeader));
    channelData->dataOffset         = 0;
    channelData->dataSize           = size;
    memcpy(((uint8_t *)channelData) + sizeof(ChannelDataHeader), data, size);
    sendHunk(buffer, bufferSize);
    vPortFree(buffer);
}

static bool sendStaticEffectData(const DisplayStaticEffect *effect)
{
    if (effect == NULL)
        return false;

    if ((effect->type != EffectIdRainbow || effect->type != EffectIdNone) &&
        (effect->paletteColor == NULL || effect->paletteColorCount == 0))
        return false;

    static const EffectId EffectIdMap[] = {
        [DisplayStaticEffectTypeNone]    = EffectIdNone,
        [DisplayStaticEffectTypeFade]    = EffectIdFade,
        [DisplayStaticEffectTypeChase]   = EffectIdChase,
        [DisplayStaticEffectTypeSwitch]  = EffectIdSwitch,
        [DisplayStaticEffectTypeRainbow] = EffectIdRainbow,
        [DisplayStaticEffectTypeFill]    = EffectIdFill,
    };
    uint32_t bufferSize = PROTOCOL_EFFECT_DATA_SIZE(effect->paletteColorCount);
    uint8_t * buffer    = (uint8_t *)pvPortMalloc(bufferSize);
    if (buffer == NULL)
        return false;

    memset((uint8_t *)buffer, 0, bufferSize);
    DataHeader * header = (DataHeader *)buffer;
    header->dataId = DataIdEffect;
    EffectDataHeader * effectHeader = (EffectDataHeader *)((uint8_t *)header + sizeof(DataHeader));
    effectHeader->paletteColorCount = effect->paletteColorCount;
    effectHeader->effectId = EffectIdMap[effect->type];
    effectHeader->lateral = effect->lateral ? 1 : 0;
    effectHeader->mirrored = effect->mirror ? 1 : 0;
    effectHeader->reverse = effect->reverse ? 1 : 0;
    effectHeader->effectPeriod = effect->periodMs;
    effectHeader->paletteColorOffset = effect->paletteColorIndex;
    EffectDataColor * effectColor = (EffectDataColor *)((uint8_t *)effectHeader + sizeof(EffectDataHeader));
    if (effectHeader->paletteColorCount > 0) {
        for (uint32_t i = 0; i < effectHeader->paletteColorCount; i++) {
            effectColor[i].r = effect->paletteColor[i].r;
            effectColor[i].g = effect->paletteColor[i].g;
            effectColor[i].b = effect->paletteColor[i].b;
        }
    }
    bool result = sendHunk(buffer, bufferSize);
    vPortFree(buffer);
    return result;
}

static bool sendConfigData(const DisplayConfig *config)
{
    if (config == NULL)
        return false;

    static const ConfigDriverType DriverTypeMap[DisplayDriverTypeCount] = {
        [DisplayDriverTypeNone]    = ConfigDriverTypeNone,
        [DisplayDriverTypePWM]     = ConfigDriverTypePwm,
        [DisplayDriverTypePixel1]  = ConfigDriverTypePixel1,
        [DisplayDriverTypePixel2]  = ConfigDriverTypePixel2,
        [DisplayDriverTypeSerial1] = ConfigDriverTypeSerial1,
        [DisplayDriverTypeSerial2] = ConfigDriverTypeSerial2,
    };
    static const uint32_t DisplayConfigKeyCount = 3;
    uint32_t bufferSize = PROTOCOL_CONFIG_PAIR_LIST_SIZE(DisplayConfigKeyCount);
    uint8_t * buffer    = (uint8_t *)pvPortMalloc(bufferSize);
    if (buffer == NULL)
        return false;

    memset((uint8_t *)buffer, 0, bufferSize);
    DataHeader * header = (DataHeader *)buffer;
    header->dataId = DataIdConfigPairList;
    ConfigPairListHeader * cfgHeader = (ConfigPairListHeader *)(( (uint8_t *)header) + sizeof(DataHeader) );
    cfgHeader->configPairCount = DisplayConfigKeyCount;
    ConfigPair * cfgPairs = (ConfigPair *)( ((uint8_t *)cfgHeader) + sizeof(ConfigPairListHeader) );
    cfgPairs[0].configKey   = ConfigKeyChannelCount;
    cfgPairs[0].configValue = config->channelCount;
    cfgPairs[1].configKey   = ConfigKeyGateOverride;
    cfgPairs[1].configValue = config->gateOverride ? 0 : 1;
    cfgPairs[2].configKey   = ConfigKeyDriverType;
    cfgPairs[2].configValue = DriverTypeMap[config->driverType];
    //cfgPairs[3].configKey   = ConfigKeyCommChannel;
    //cfgPairs[3].configValue = DefaultCommChannel;
    bool result = sendHunk(buffer, bufferSize);
    vPortFree(buffer);
    return result;
}

static void rfSentCb(bool success)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(txSemaphore, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    txSuccess = success;
}

static void rfReceivedCb(const uint8_t *data, size_t size)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(rxSemaphore, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    if (size > sizeof(rxData)) {
        size = sizeof(rxData);
    }

    memcpy(rxData, data, size);
    rxDataLen = size;
}

static bool sendHunk(uint8_t *data, size_t size)
{
#define START_CHAR (0x80U)
#define END_CHAR (0x40U)
#define SEQ_MASK (0x3FU)

    if (data == NULL || size == 0) {
        return false;
    }

    xSemaphoreTake(radioMutex, portMAX_DELAY);
    static uint8_t packetBuffer[RADIO_DATA_MAX_LENGTH];
    bool success = true;

    uint8_t sequence = 0;
    do {
        const uint32_t payloadSize = size > sizeof(packetBuffer) - 1
                                    ? sizeof(packetBuffer) - 1
                                    : size;
        packetBuffer[0] = 0;
        if (sequence == 0) {
            packetBuffer[0] |= START_CHAR;
        }
        if (payloadSize == size) {
            packetBuffer[0] |= END_CHAR;
        }
        packetBuffer[0] |= sequence & SEQ_MASK;
        memcpy(&packetBuffer[1], data, payloadSize);

        txSuccess = false;
        rxDataLen = 0;
        rxData[0] = 0;

        radioSendData(packetBuffer, payloadSize + 1);
        xSemaphoreTake(txSemaphore, portMAX_DELAY);
        if (!txSuccess) {
            success = false;
            break;
        }
        if (xSemaphoreTake(rxSemaphore, pdMS_TO_TICKS(ACK_TIMEOUT_MS)) != pdPASS) {
            success = false;
            break;
        }
        if (rxDataLen == 0 || rxData[0] != 0xAA) {
            success = false;
            break;
        }
        vTaskDelay(pdMS_TO_TICKS(10));
        data = (uint8_t *)(((uint32_t)data) + payloadSize);
        size -= payloadSize;
        sequence++;
    } while (size);
    xSemaphoreGive(radioMutex);
    return success;
#undef START_CHAR
#undef END_CHAR
#undef SEQ_MASK
}

static void rfInit(void)
{
    uint8_t address[ADDR_LENGTH];
    uint8_t uid[BOARD_CHIP_ID_LEN];
    boardGetChipUid(uid);
    memcpy(address, uid, sizeof(address));
    xSemaphoreTake(radioMutex, portMAX_DELAY);
    radioDisable();
    radioInit(rfReceivedCb, rfSentCb, false, address, DefaultCommChannel);
    xSemaphoreTake(rxSemaphore, 0);
    xSemaphoreTake(txSemaphore, 0);
    xSemaphoreGive(radioMutex);
}