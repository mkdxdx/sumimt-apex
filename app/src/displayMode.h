#pragma once

typedef enum DisplayMode {
    DisplayModeNone,     // display disabled
    DisplayModeLinked,   // display is viewport data bound and its data updates in local render loop
    DisplayModeDetached, // display data is updated on demand and contains an effect that it renders by itself
    DisplayModeCount
} DisplayMode;
