#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "color.h"
#include "common.h"

typedef enum NumericValueType {
    NumericValueTypeU8,
    NumericValueTypeI8,
    NumericValueTypeU16,
    NumericValueTypeI16,
    NumericValueTypeU32,
    NumericValueTypeI32,
    NumericValueTypeCount,
} NumericValueType;

typedef enum NumericBase {
    NumericBaseBin = 2,
    NumericBaseOct = 8,
    NumericBaseDec = 10,
    NumericBaseHex = 16
} NumericBase;

typedef struct ColorPickerSettings {
    bool isHsv;
    union {
        RgbColor *rgbColors;
        HsvColor *hsvColors;
    };
    uint32_t colorCount;
    const Point2d *position;
    const Point2d *size;
    uint32_t colorElementHeight;
    uint32_t colorElementWidth;
    uint32_t colorIndex;
    bool inFocus;
} ColorPickerSettings;

void guiElementInitScreen(void);
void guiElementFillBackground(void);
void guiElementColorPicker(ColorPickerSettings *settings);
void guiElementCheckBox(const char *label, bool isChecked, bool inFocus, 
                        const Point2d *position, const Point2d *size);
void guiElementLabeledNumericValue(void *value, NumericValueType type, const char *label, bool inFocus, NumericBase base,
                                   const Point2d *position, const Point2d *size);
void guiElementLabel(const char * label, bool inFocus, 
                     const Point2d *position, const Point2d *size);
void guiElementList(const char * strings[], uint32_t count, int32_t index, const char * marker, 
                    const Point2d *position, const Point2d *size);
void guiElementLabelGetSize(const char *label, Point2d *size);
void guiElementLabeledString(const char *label, const char *string, bool inFocus, 
                             const Point2d *position, const Point2d *size);